import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
// import style from "./css/page.module.css";
// import axios from 'axios';

const PricingComponent = () => {
  const [dayPrice, setDayPrice] = useState(1000);
  const [nightPrice, setNightPrice] = useState(1500);
  const [editNight, setEditNight] = useState(false);
  const [editDay, setEditDay] = useState(false);

  const handleDayEditClick = () => {
    console.log("Edit button pressed!");
    setEditDay(true);
  };

  const handleDayCancelClick = () => {
    setEditDay(false);
  };

  const handleNightEditClick = () => {
    setEditNight(true);
  };

  const handleNightCancelClick = () => {
    setEditNight(false);
  };

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
          width: "100vw",
        }}
      >
        <div
          style={{
            width: "100%",
            height: "100%",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <div
            style={{
              border: "1px solid blue",
              width: "61%",
              paddingTop: "10rem",
              paddingBottom: "10rem",
              paddingLeft: "10rem",
              paddingRight: "15rem",
            }}
          >
            <h4
              style={{
                width: "100%",
                textAlign: "start",
                paddingTop: "2rem",
                paddingBottom: "2rem",
              }}
            >
              Ground Price
            </h4>
            <div style={{ width: "100%" }}>
              <div style={{ width: "100%" }}>
                <label
                  style={{
                    textAlign: "start",
                    width: "100%",
                    fontWeight: "bold",
                    paddingBottom: "1rem",
                  }}
                >
                  Day Pricing
                </label>
                {editDay ? (
                  <div className="d-flex flex-column" style={{ gap: "2rem" }}>
                    <input
                      type="text"
                      value={dayPrice}
                      onChange={(e) => setDayPrice(e.target.value)}
                      className="px-20"
                    />
                    <div
                      className="d-flex align-items-center justify-content-center"
                      style={{ gap: "1rem" }}
                    >
                      <button
                        onClick={handleDayCancelClick}
                        style={{
                          padding: "1rem",
                          borderRadius: "5px",
                          color: "white",
                          background: "#2c73eb",
                        }}
                      >
                        Save
                      </button>
                      <button
                        onClick={handleDayCancelClick}
                        style={{
                          padding: "1rem",
                          borderRadius: "5px",
                          color: "white",
                          background: "#fd5757",
                        }}
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                ) : (
                  <div
                    className="d-flex"
                    style={{ width: "100%", gap: "1rem", height: "100%" }}
                  >
                    <p
                      style={{
                        border: "1px solid black",
                        textAlign: "start",
                        paddingTop: "1rem",
                        paddingBottom: "1rem",
                        paddingLeft: "1rem",
                        borderRadius: "5px",
                        margin: 0,
                        width: "75%",
                      }}
                    >
                      {dayPrice}/hour
                    </p>
                    <button
                      onClick={handleDayEditClick}
                      style={{
                        paddingLeft: "1rem",
                        paddingRight: "1rem",
                        borderRadius: "5px",
                        border: "1px solid black",
                      }}
                    >
                      Edit
                    </button>
                  </div>
                )}
              </div>
              <div style={{ paddingTop: "2rem" }}>
                <label
                  style={{
                    textAlign: "start",
                    width: "100%",
                    fontWeight: "bold",
                    paddingBottom: "1rem",
                  }}
                >
                  Night Pricing
                </label>
                {editNight ? (
                  <div className="d-flex flex-column" style={{ gap: "2rem" }}>
                    <input
                      type="text"
                      value={nightPrice}
                      onChange={(e) => setNightPrice(e.target.value)}
                    />
                    <div
                      className="d-flex align-items-center justify-content-center"
                      style={{ gap: "1rem" }}
                    >
                      <button
                        onClick={handleNightCancelClick}
                        style={{
                          padding: "1rem",
                          borderRadius: "5px",
                          color: "white",
                          background: "#2c73eb",
                        }}
                      >
                        Save
                      </button>
                      <button
                        onClick={handleNightCancelClick}
                        style={{
                          padding: "1rem",
                          borderRadius: "5px",
                          color: "white",
                          background: "#fd5757",
                        }}
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                ) : (
                  <div
                    className="d-flex"
                    style={{ width: "100%", gap: "1rem", height: "100%" }}
                  >
                    <p
                      style={{
                        border: "1px solid black",
                        textAlign: "start",
                        paddingTop: "1rem",
                        paddingBottom: "1rem",
                        paddingLeft: "1rem",
                        borderRadius: "5px",
                        margin: 0,
                        width: "75%",
                      }}
                    >
                      {nightPrice}/hour
                    </p>
                    <button
                      onClick={handleNightEditClick}
                      style={{
                        paddingLeft: "1rem",
                        paddingRight: "1rem",
                        borderRadius: "5px",
                        border: "1px solid black",
                      }}
                    >
                      Edit
                    </button>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PricingComponent;
