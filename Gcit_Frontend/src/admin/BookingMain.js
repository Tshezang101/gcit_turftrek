import React from 'react'

import 'bootstrap/dist/css/bootstrap.min.css'

import 'bootstrap-icons/font/bootstrap-icons.css'

import Sidebar from './components/Sidebar'

import Booking from './components/Booking'

import { useState } from 'react'



function BookingMain() {
    const [toggle, setToggle] = useState(true)
    const Toggle = () => { setToggle(!toggle) }
    return (<div className='container-fluid bg-white min-vh-100 '>
        <div className='row '>
            {toggle && <div className='col-4 col-md-2  vh-100 position-fixed'>
                <Sidebar /> </div>}
            {toggle && <div className='col-4 col-md-2'></div>}
            <div className='col'>
                <Booking Toggle={Toggle} /> </div>
        </div>
    </div>
    )
}
export default BookingMain;