import axios from "axios";
import convertDateFormat from "../utility/dateFormatter";

export const getBookingAPI = async () => {
  return await axios.get(
    `http://localhost:8080/admin/generalbookings/2023-11-27`
  );
};

export const getIndividualBookingAPI = async (id) => {
  return await axios.get(`http://localhost:8080/admin/singlebooking/${id}`);
};

// check
export const getfreeSlotsAPI = async (date, duration) => {
  return await axios.get(
    `http://localhost:8080/admin/booking/${date}/${duration}`
  );
};

export const getCancellAPI = async (id) => {
  return await axios.get(
    `http://localhost:8080/admin/booking/cancellation/details/${id}`
  );
};

export const CancellAPI = async (id, data) => {
  return await axios.put(
    `http://localhost:8080/admin/booking/cancellation/${id}`,
    data
  );
};

export const bookGround = async (data) => {
  return await axios.post(`http://localhost:8080/admin/booking`, data);
};

export const editCredentialAPI = async (id, data) => {
  return await axios.put(`http://localhost:8080/admin/credential/${id}`, data);
};

export const paymentAPI = async (data) => {
  return await axios.put(`http://localhost:8080/admin/payment`, data);
};
export const rescheduleAPI = async (id, data) => {
  return await axios.put(
    `http://localhost:8080/admin/booking/update/${id}`,
    data
  );
};

export const dueBookingsAPI = async () => {
  return await axios.get(`http://localhost:8080/admin/duebookings`);
};
