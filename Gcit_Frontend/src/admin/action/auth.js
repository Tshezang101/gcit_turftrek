import axios from "axios";

export const adminLoginAPI = async (admin) => {
  return await axios.post("http://localhost:8080/adminlogin", admin);
};

export const forgotPasswordAPI = async () => {
  return await axios.post("http://localhost:8080/admin/forgotPassword");
};

export const passwordResetAPI = async (password, token) => {
  return await axios.post(
    `http://localhost:8080/admin/passwordReset/${token}`,
    { password }
  );
};

export const usercountAPI = async (usercount) => {
  return await axios.post("http://localhost:8080/usercount", usercount);
};
