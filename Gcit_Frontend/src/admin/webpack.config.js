// webpack.config.js

const webpack = require('webpack');
module.exports = {
  // other webpack config options...
  devServer: {
    hot: true,
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
  ],
};
