import { useState } from "react";
import { toast } from "react-toastify";
import LoginForm from "../components/LoginForm";
import { useNavigate } from "react-router-dom";
import { adminLoginAPI, forgotPasswordAPI } from "../action/auth";
import { useAuth } from "../contexts/AuthContext"; // Use useAuth hook

const AdminLogin = () => {
  const [email, setEmail] = useState(""); // State for email input
  const [password, setPassword] = useState(""); // State for password input
  const navigate = useNavigate(); // Router navigation
  const { login } = useAuth(); // Use context if you are using it for auth state

  // Handle form submission for login
  const handleSubmit = async () => {
    console.log("Submitting login");
    try {
      const res = await adminLoginAPI({ email, password });
      console.log("Login response:", res);

      if (res && res.status) {
        toast.success("Login Successful!");
        login(); // Update AuthContext state
        localStorage.setItem("token", res.data.token);
        const token = localStorage.getItem("token");
        console.log(token);
        console.log("Navigating to Dashboard");
        navigate("/Dashboard");
      } else {
        console.log("Login failed");
        toast.error("Login failed. Please check your credentials.");
      }
    } catch (err) {
      console.error("Error during login:", err);
      toast.error("Login failed");
    }
  };

  // Handle "Forgot Password" action
  const handleForgotPassword = async () => {
    try {
      const res = await forgotPasswordAPI();

      if (res && res.status) {
        navigate("/adminEmailnote"); // Navigate to the "forgot password" note
      }
    } catch (err) {
      // Error handling with proper checks
      if (err.response && err.response.status === 400) {
        toast.error(err.response.data.message); // Specific error message
      } else {
        toast.error("An unexpected error occurred."); // Default error message
      }
    }
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-6 offset-md-3">
          <LoginForm
            handleSubmit={handleSubmit}
            email={email}
            setEmail={setEmail}
            password={password}
            setPassword={setPassword}
            forgotPasswordHandler={handleForgotPassword}
          />
        </div>
      </div>
    </div>
  );
};

export default AdminLogin;
