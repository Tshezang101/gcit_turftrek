import React from "react";
import { useForm } from "react-hook-form";
import email from "../image/email.png"
// import "../css";
import { Link } from "react-router-dom";
import styles from "../css/log.module.css"

export default function AdminNote() {
    const { handleSubmit } = useForm();

    const onSubmit = (data) => console.log(data);
    return (

        <div className={styles.form}>
            <p className={styles.title} >Check Your Email!</p>
            {/* < p className="subtitle"> Please fill out below form to reset your password</p> */}
            <img src={email} alt="#" style={{ width: 250, height: 200, margin: 20, marginLeft: 80, }} />
            < p className={styles.subtitle} style={{ marginTop: 45 }}> Please check your email, we have send you a password reset form !</p>
            <form className={styles.App} onSubmit={handleSubmit(onSubmit)}>


                {/* <input type={"Submit"} style={{ backgroundColor: "#2C73EB", width: 300, marginLeft: 87, borderRadius: 20 }}
                /> */}
                <Link to="/admin">
                    <button style={{ backgroundColor: "#2C73EB", width: 300, marginLeft: 87, borderRadius: 20, height: 50 }}>
                        Done
                    </button>
                </Link>

            </form >
        </div>

    );
}