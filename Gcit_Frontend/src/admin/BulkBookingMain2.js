import React from 'react'

import 'bootstrap/dist/css/bootstrap.min.css'

import 'bootstrap-icons/font/bootstrap-icons.css'

import Sidebar from './components/Sidebar'

import BBookingt from './components/BulkBooking2'

import { useState } from 'react'



function BBookingMain2() {
    const [toggle, setToggle] = useState(true)
    const Toggle = () => { setToggle(!toggle) }
    return (<div className='container-fluid bg-white min-vh-100 '>
        <div className='row '>
            {toggle && <div className='col-4 col-md-2 vh-100 position-fixed'>
                <Sidebar /> </div>}
            {toggle && <div className='col-4 col-md-2'></div>}
            <div className='col'>
                <BBookingt Toggle={Toggle} /> </div>
        </div>
    </div>
    )
}
export default BBookingMain2;