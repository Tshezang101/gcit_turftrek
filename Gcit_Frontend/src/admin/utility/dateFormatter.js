function convertDateFormat(inputDate) {
    // Split the input date into month, day, and year
    const [month, day, year] = inputDate.split('/');

    // Create a new date object with the given components
    const convertedDate = new Date(`${year}-${month}-${day}`);

    // Get the components of the converted date
    const convertedYear = convertedDate.getFullYear();
    const convertedMonth = String(convertedDate.getMonth() + 1).padStart(2, '0'); // Months are zero-based
    const convertedDay = String(convertedDate.getDate()).padStart(2, '0');

    // Concatenate the components to form the desired format
    const result = `${convertedYear}-${convertedMonth}-${convertedDay}`;

    return result;
}

export default convertDateFormat;