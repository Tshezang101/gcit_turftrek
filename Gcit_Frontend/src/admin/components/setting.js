import React, { useState, useEffect } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import "../css/setting.css";

const PricingComponent = () => {
  const [dayTime, setDayTime] = useState(0);
  const [nightTime, setNightTime] = useState(0);
  const [dayPrice, setDayPrice] = useState(0);
  const [nightPrice, setNightPrice] = useState(0);
  const [originalDayPrice, setOriginalDayPrice] = useState(0);
  const [originalNightPrice, setOriginalNightPrice] = useState(0);
  const [isEditing, setIsEditing] = useState(false);
  const [confirmUpdate, setConfirmUpdate] = useState(false);
  const [file, setfile] = useState("");
  const [image, setImage] = useState("");

  useEffect(() => {
    fetchData();
  }, []);

  function previewFiles(file) {
    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onloadend = () => {
      console.log(image);
      setImage(reader.result);
    };
  }
  const handleChange = (e) => {
    const file = e.target.files[0];
    setfile(file);
    previewFiles(file);
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      if (file.size > 10 * 1024 * 1024) {
        throw new Error("File size exceeds the maximum allowed size.");
      }
      const result = await axios.post("http://localhost:8080/uploadImage", {
        image: image,
      });
      // Display success message if upload is successful
      alert("Upload successful!");
      // Handle the result if needed
    } catch (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        console.error(
          "Server responded with status code:",
          error.response.status
        );
      } else if (error.request) {
        // The request was made but no response was received
        console.error("No response received from server.");
      } else {
        // Something happened in setting up the request that triggered an error
        console.error("Error setting up request:", error.message);
      }
    }
  };

  const fetchData = async () => {
    try {
      const response = await axios.get("http://localhost:8080/price");
      setDayTime(response.data.daytime);
      setNightTime(response.data.nighttime);
      setDayPrice(response.data.dayprice);
      setNightPrice(response.data.nightprice);
      // Store original prices for comparison
      setOriginalDayPrice(response.data.dayprice);
      setOriginalNightPrice(response.data.nightprice);
    } catch (error) {
      console.error("Error fetching pricing data:", error);
    }
  };

  const handleUpdateClick = () => {
    setIsEditing(true);
  };

  const handleDoneClick = () => {
    setConfirmUpdate(true);
  };

  const handleConfirmUpdate = async () => {
    try {
      await axios.patch("http://localhost:8080/price", {
        daytime: dayTime,
        nighttime: nightTime,
        dayprice: dayPrice,
        nightprice: nightPrice,
      });
      setConfirmUpdate(false);
      setIsEditing(false);
      // Optionally, you can show a success message or update state to reflect changes
    } catch (error) {
      console.error("Error updating pricing data:", error);
    }
  };

  const handleCancelUpdate = () => {
    // Reset prices to original values
    setDayPrice(originalDayPrice);
    setNightPrice(originalNightPrice);
    setConfirmUpdate(false);
    setIsEditing(false);
  };

  return (
    <div className="px-3 bg-white col-lg-12">
      <div
        style={{
          position: "fixed",
          top: 0,
          left: 240,
          right: 0,
          height: "90px",
          backgroundColor: "white",
          zIndex: 1000,
          borderBottom: "2px solid green",
          padding: "10px 20px",
          display: "flex",
          alignItems: "center",
        }}
      >
        <h1
          style={{
            color: "#052240",
            fontWeight: "bold",
            fontSize: "22px",
            fontWeight: "bold",
            fontSize: "30px",
            textAlign: "justify",
            marginTop: "3%",
            marginLeft: "1%",
          }}
        >
          Setting
        </h1>
      </div>
      <div className="px-3 bg-white col-lg-12" style={{ paddingTop: "30px" }}>
        <div className="container-fluid card-container">
          <div
            className="row g-15 my-5"
            style={{ paddingTop: 50, marginLeft: 40 }}
          >
            <table
              style={{
                width: "100%",
                height: "100%",
                marginRight: "-20px",
                marginLeft: "-10px",
                backgroundColor: "white",
                borderRadius: "1px",
                border: "2px solid #d3d3d3",
              }}
            >
              <thead>
                <tr
                  style={{
                    position: "sticky",
                    top: 0,
                    backgroundColor: "white",
                    zIndex: 1,
                    paddingBottom: "10px",
                  }}
                >
                  <th style={headerStyle}>Days</th>
                  <th style={headerStyle}>Hours</th>
                  <th style={headerStyle}>Rate (Nu.)</th>
                </tr>
              </thead>
              <tbody>
                <tr style={rowStyle}>
                  <td style={cellStyle}>Weekends</td>
                  <td style={cellStyle}>
                    {isEditing ? (
                      <input
                        type="text"
                        value={dayTime}
                        onChange={(e) => setDayTime(e.target.value)}
                      />
                    ) : (
                      dayTime
                    )}
                  </td>
                  <td style={cellStyle}>
                    <input
                      type="text"
                      value={dayPrice}
                      onChange={(e) => setDayPrice(e.target.value)}
                      disabled={!isEditing}
                    />
                  </td>
                </tr>
                <tr style={rowStyle}>
                  <td style={cellStyle}>Weekends</td>
                  <td style={cellStyle}>
                    {isEditing ? (
                      <input
                        type="text"
                        value={nightTime}
                        onChange={(e) => setNightTime(e.target.value)}
                      />
                    ) : (
                      nightTime
                    )}
                  </td>
                  <td style={cellStyle}>
                    <input
                      type="text"
                      value={nightPrice}
                      onChange={(e) => setNightPrice(e.target.value)}
                      disabled={!isEditing}
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div
          className="button-container"
          style={{ paddingBottom: "40PX", paddingTop: "1px" }}
        >
          {!isEditing && (
            <button
              className="btn btn-primary"
              onClick={handleUpdateClick}
              style={{ margin: "0 auto" }}
            >
              Update
            </button>
          )}
          {isEditing && (
            <>
              <button className="btn btn-primary" onClick={handleDoneClick}>
                Done
              </button>
              <button
                className="btn btn-secondary"
                onClick={handleCancelUpdate}
              >
                Cancel
              </button>
            </>
          )}
        </div>
      </div>

      <div>
        {confirmUpdate && <div className="overlay"></div>}
        {confirmUpdate && (
          <div className="confirmation-modal">
            <p>Are you sure you want to update the prices?</p>
            <button className="btn btn-primary" onClick={handleConfirmUpdate}>
              Yes
            </button>
            <button className="btn btn-secondary" onClick={handleCancelUpdate}>
              No
            </button>
          </div>
        )}
      </div>

      <div
        style={{
          textAlign: "center",
          // background: "#f5f5f5",
          padding: "20px",
          background: "white",
          // marginBottom: "-20px",
          width: "90%",
          height: "150px",
          marginLeft: "50px",
          paddingTop: "10px",
          borderRadius: "5px",
          border: "2px solid #ddd",
        }}
      >
        <form onSubmit={handleSubmit}>
          <label htmlFor="fileInput" style={{ display: "block" }}>
            <strong>UPLOAD BANNER</strong>
          </label>
          <div style={{ marginTop: "15px" }}>
            <input
              type="file"
              id="fileInput"
              onChange={handleChange}
              required
              accept="image/png, image/jpeg, image/jpg, image/jfif"
              style={{ display: "none" }}
            />
            <label
              htmlFor="fileInput"
              className="btn btn-primary"
              style={{ background: "#FFB90A", border: "none" }}
            >
              Choose File
            </label>
            {file && (
              <span style={{ marginLeft: "10px" }}>
                Selected file: {file.name}
              </span>
            )}
          </div>
          {file && (
            <button
              type="submit"
              className="btn btn-success"
              style={{
                background: "#003245",
                marginTop: "10px",
                border: "none",
              }}
            >
              Upload
            </button>
          )}
        </form>
      </div>
    </div>
  );
};

export default PricingComponent;
const headerStyle = {
  position: "sticky",
  top: "90px",
  left: "0",
  right: "0",
  padding: "6px",
  textAlign: "center",
  backgroundColor: "white",
  fontWeight: "bold",
  zIndex: 1,
  height: "80px",
  borderBottom: "1px solid green",
};

const cellStyle = {
  padding: "6px",
  textAlign: "center",
  border: "1px",
  paddingTop: "30px",
  height: "80px",
  backgroundColor: "white",
};

const rowStyle = {
  // backgroundColor: "#e6f7ff",
  borderBottom: "1px solid #ddd",
  cursor: "pointer",
  transition: "background-color 0.3s",
  backgroundColor: "white",
};
