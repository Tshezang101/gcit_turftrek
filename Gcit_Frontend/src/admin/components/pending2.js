import React from 'react'


import 'react-calendar/dist/Calendar.css';
import 'bootstrap-icons/font/bootstrap-icons.css';
import style from "../css/page.module.css"
import 'reactjs-popup/dist/index.css';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { getIndividualBookingAPI, paymentAPI } from "../action/booking";
import { useState, useEffect } from 'react';
import { toast } from "react-toastify";
import Popup from 'reactjs-popup';

function Pendingt({ Toggle }) {
    const { id } = useParams();
    const [indBook, setindBook] = useState({});
    const [paymentType, setPaymentType] = useState('');
    const [bankName, setBankName] = useState('');
    const [journalNumber, setJournalNumber] = useState('');

    useEffect(() => {
        const getBookings = async () => {
            try {
                const res = await getIndividualBookingAPI(id);
                setindBook(res.data.data)
            } catch (error) {
                console.log(error)
            }
        };

        getBookings();
    }, []);

    return (
        <div className={style.body}>
            <div className='px-3 bg-white col-lg-12'>
                <div className='container-fluid card-container '>
                    <div className='row g-15 my-3 ' style={{ marginLeft: '100px', width: '1044.766 px' }}>
                        <div className={style.Pg2buttons}>
                            <Link to='/booking'> <button style={{
                                borderRadius: '10px',
                                backgroundColor: '#2c73eb',
                                width: '115px',
                                height: '42px',
                                color: 'white'
                            }}>Back</button></Link>
                            {/* <button className={style.addBtn}>Action</button> */}
                            <ul className="navbar-nav ms-auto mt-2 mt-lg-0">
                                <li className="nav-item dropdown">
                                    <button className={style.addBtn} to="#" id="dropdownId" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Action </button>   <div className="dropdown-menu" aria-labelledby="dropdownId" >
                                        {/* <Link className="dropdown-item" to="#">Profile</Link> */}
                                        <Popup
                                            trigger={<Link className="dropdown-item" to="#" >Payment</Link>}
                                            modal
                                            nested
                                            contentStyle={{
                                                height: '400px', display: 'inline-flex', width: '650px',
                                                padding: '20px 20px 20px 5px',
                                                flexDirection: 'column',

                                                gap: '68.722px',
                                                background: 'var(--dark-text-ffffff, #fff)'
                                            }}
                                        >
                                            {close => (
                                                <div >
                                                    <form
                                                        id="paymentForm"
                                                        style={{ height: '400px', marginLeft: '100px' }}
                                                        onSubmit={async (e) => {
                                                            e.preventDefault();

                                                            try {
                                                                const paymentData = {
                                                                    "bookingId": id,
                                                                    "match_status": "played",
                                                                    "payment_status": "paid",
                                                                    "payment_journal_number": journalNumber,
                                                                    "payment_bank_name": bankName,
                                                                    "payment_type": paymentType
                                                                }

                                                                const res = await paymentAPI(paymentData);
                                                                console.log(res.data)
                                                                if (res.data.status === "Success") {
                                                                    toast.success("Success");
                                                                    close();
                                                                }
                                                            } catch (error) {
                                                                console.log(error)
                                                            }
                                                        }}
                                                    >
                                                        <button
                                                            style={{
                                                                height: '20px',
                                                                width: '20px',
                                                                marginLeft: '450px',
                                                                alignItems: 'center',
                                                                paddingBottom: '25px',
                                                                paddingRight: '15px',
                                                            }}
                                                            id="close-btn"
                                                            onClick={() => close()}
                                                        >
                                                            &times;
                                                        </button>

                                                        <div className={style.formGroup}>
                                                            <label htmlFor="ptype">Payment Type</label>
                                                            <select
                                                                style={{ width: '60vh', height: '7vh' }}
                                                                value={paymentType}
                                                                onChange={(e) => setPaymentType(e.target.value)}
                                                            >
                                                                <option value="cash">Cash</option>
                                                                <option value="online">Online</option>
                                                            </select>
                                                        </div>
                                                        <div className={style.formGroup}>
                                                            <label htmlFor="Bnumber">Bank Name</label>
                                                            <input
                                                                type="text"
                                                                name='Bnumber'
                                                                value={bankName}
                                                                onChange={(e) => setBankName(e.target.value)}
                                                            />
                                                        </div>
                                                        <div className={style.formGroup}>
                                                            <label htmlFor="Jnumber">Journal Number</label>
                                                            <input
                                                                type="number"
                                                                name="Jnumber"
                                                                placeholder=""
                                                                value={journalNumber}
                                                                onChange={(e) => setJournalNumber(e.target.value)}
                                                            />
                                                        </div>

                                                        <div className={style.ubutton}>
                                                            <button type="submit">
                                                                <p> Update </p>
                                                            </button>
                                                            <button style={{ marginLeft: '18vh' }} onClick={() => close()}>
                                                                <p> Cancel</p>
                                                            </button>
                                                        </div>
                                                    </form>

                                                </div>
                                            )}
                                        </Popup>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div className={style.MainContent2}>
                            <div className={style.firstCol}>

                                <div className={style.firstcola}>
                                    <h4 style={{ marginRight: 50, alignSelf: "flex-start" }}>Personal Information</h4>
                                    <div className='personalInfo'>
                                        <div>
                                            <h6 style={{ marginRight: 230, alignSelf: "flex-start" }}>Name</h6>
                                            <p className='Name' style={{ marginRight: 140, alignSelf: "flex-start" }}>{indBook.user_name}</p>
                                        </div>
                                        <div>
                                            <h6 style={{ marginRight: 170, alignSelf: "flex-start" }}>Phone Number</h6>
                                            <p className='Phone' style={{ marginRight: 210, alignSelf: "flex-start" }}>{indBook.phone_number}</p>
                                        </div>
                                        <div >
                                            <h6 style={{ marginRight: 240, alignSelf: "flex-start" }}>Email</h6>
                                            <p className='Duration' style={{ marginRight: 80, alignSelf: "flex-start" }}>{indBook.email}</p>
                                        </div>


                                    </div>
                                </div>
                                <div className={style.firstcolb}>
                                    <h4 style={{ marginRight: 60, alignSelf: "flex-start" }}>Booking Information</h4>
                                    <div className='TicketInput'>
                                        <h6 style={{ marginRight: 220, alignSelf: "flex-start" }}>TicketID</h6>
                                        <p className='TicketID' style={{ marginRight: 270, alignSelf: "flex-start" }}>{indBook.id}</p>
                                    </div>
                                    <div className='DInput'>
                                        <h6 style={{ marginRight: 220, alignSelf: "flex-start" }}>Duration</h6>
                                        <p className='Duration' style={{ marginRight: 270, alignSelf: "flex-start" }}>{indBook.match_duration}</p>
                                    </div>
                                    <div className='TInput'>
                                        <h6 style={{ marginRight: 220, alignSelf: "flex-start" }}>Start Time</h6>
                                        <p className='Time' style={{ marginRight: 240, alignSelf: "flex-start" }}>{indBook.match_start_time}</p>
                                    </div>
                                    <div className='DateInput'>

                                        <h6 style={{ marginRight: 265, alignSelf: "flex-start" }}>Date</h6>
                                        <p className='Date' style={{ marginRight: 110, alignSelf: "flex-start" }}>{indBook.match_date}</p>

                                    </div>
                                    <div className='StatusInput'>

                                        <h6 style={{ marginRight: 260, alignSelf: "flex-start" }}>Status</h6>
                                        <p className='status' style={{ marginRight: 240, alignSelf: "flex-start" }}>{indBook.match_status}</p>

                                    </div>
                                </div>
                            </div>
                            <div className={style.secondCol}>
                                <h4 style={{ marginRight: 100, alignSelf: "flex-start" }}>Payment Information</h4>
                                <div className='PayInfo'>
                                    <div >
                                        <h6 style={{ marginRight: 210, alignSelf: "flex-start" }}>Financial Status</h6>
                                        <p className='FS' style={{ marginRight: 270, alignSelf: "flex-start" }}>{indBook.payment_status}</p>
                                    </div>
                                    <div>
                                        <h6 style={{ marginRight: 270, alignSelf: "flex-start" }}>Amount</h6>
                                        <p className='Amount' style={{ marginRight: 300, alignSelf: "flex-start" }}>{indBook.payment_amount}</p>
                                    </div>
                                    <div>
                                        <h6 style={{ marginRight: 240, alignSelf: "flex-start" }}>Match Status</h6>
                                        <p className='TS' style={{ marginRight: 260, alignSelf: "flex-start" }}>{indBook.match_status}</p>
                                    </div>
                                    <div >
                                        <h6 style={{ marginRight: 230, alignSelf: "flex-start" }}>Payment Date</h6>
                                        <p className='PD' >{indBook.payment_date}</p>
                                    </div>
                                    <div >
                                        <h6 style={{ marginRight: 210, alignSelf: "flex-start" }}>Journal Number</h6>
                                        <p className='JN'>{indBook.payment_journal_number}</p>
                                    </div>
                                    <div >
                                        <h6 style={{ marginRight: 220, alignSelf: "flex-start" }}>Bank Account</h6>
                                        <p className='BA'>{indBook.payment_bank_name}</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >)
}

export default Pendingt;