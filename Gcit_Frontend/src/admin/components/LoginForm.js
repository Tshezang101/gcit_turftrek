import { useState } from "react";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import logo from "../image/logo.png";

const LoginForm = ({
  handleSubmit,
  email,
  setEmail,
  password,
  setPassword,
  forgotPasswordHandler,
}) => {
  const isValidEmail = (email) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const onSubmit = (e) => {
    if (e && typeof e.preventDefault === "function") {
      e.preventDefault();
    } else {
      throw new Error(
        "Event object is undefined or doesn't have preventDefault"
      );
    }

    if (!isValidEmail(email)) {
      toast.error("Enter a valid Email");
      return;
    }

    if (!password) {
      toast.error("Password cannot be empty");
      return;
    }

    if (handleSubmit) {
      handleSubmit();
    }
  };

  return (
    <div
      style={{
        maxWidth: "400px",
        margin: "0 auto",
        padding: "20px",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <div
        style={{
          textAlign: "center",
          marginBottom: "20px",
          marginTop: "60px",
          display: "flex",
          marginRight: "40px",
        }}
      >
        <img
          src={logo}
          alt="Logo"
          style={{ width: "100px", height: "100px", marginBottom: "10px" }}
        />
        <p
          style={{
            fontSize: "30px",
            color: "#003E59",
            fontWeight: "700",
            margin: "5px 0",
            marginTop: "56px",
          }}
        >
          GCIT_TURFTREK
        </p>
      </div>
      <p
        style={{
          fontSize: "24px",
          color: "#004B6B",
          fontWeight: "600",
          margin: "10px 0",
        }}
      >
        Log in to your Account
      </p>
      <form onSubmit={onSubmit} style={{ width: "100%", marginTop: "-10px" }}>
        <input
          type="email"
          id="email"
          name="email"
          placeholder="Email"
          value={email}
          onChange={handleEmailChange}
          style={{
            width: "100%",
            padding: "10px",
            margin: "10px 0",
            backgroundColor: "#FAE7CB",
            borderRadius: "5px",
            border:
              isValidEmail(email) || !email
                ? "1px solid #ced4da"
                : "1px solid red",
          }}
        />
        {!isValidEmail(email) && email && (
          <div style={{ color: "red", fontSize: "14px", marginTop: "-10px" }}>
            Enter a valid email address.
          </div>
        )}
        <input
          type="password"
          id="password"
          name="password"
          placeholder="Password"
          value={password}
          onChange={handlePasswordChange}
          style={{
            width: "100%",
            padding: "10px",
            margin: "10px 0",
            borderRadius: "5px",
            backgroundColor: "#FAE7CB",
            border: password ? "1px solid #ced4da" : "1px solid red",
          }}
        />
        {!password && (
          <div
            style={{ color: "red", fontSize: "14px", marginTop: "-10px" }}
          ></div>
        )}
        <button
          type="submit"
          style={{
            width: "100%",
            padding: "10px",
            margin: "10px 0",
            backgroundColor: "#004B6B",
            color: "white",
            border: "none",
            cursor: "pointer",
            borderRadius: "5px",
          }}
        >
          Login
        </button>
      </form>
    </div>
  );
};

export default LoginForm;
