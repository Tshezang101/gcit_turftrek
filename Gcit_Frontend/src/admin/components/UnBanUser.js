import React, { useEffect, useState } from "react";
import { useParams, useNavigate, Link } from "react-router-dom";
import axios from "axios";
import style from "../css/page.module.css";
import Popup from "reactjs-popup";
import { toast } from "react-toastify";
import "../css/setting.css";

const UnbanUser = () => {
  const [userInfo, setUserInfo] = useState(null);
  const { userId } = useParams();
  const navigate = useNavigate(); // Use the navigate hook

  useEffect(() => {
    const fetchUserInfo = async () => {
      try {
        const response = await axios.get(
          `http://localhost:8080/user/reason/${userId}`
        );
        setUserInfo(response.data);
      } catch (error) {
        console.error("Error fetching user data:", error);
        toast.error("Failed to fetch user data");
      }
    };

    fetchUserInfo();
  }, [userId]);

  const handleUnbanUser = async (e) => {
    e.preventDefault(); // Prevent the default form submission
    try {
      await axios.post(`http://localhost:8080/users/unban/${userId}`);

      // Fetch updated user information after unbanning
      const response = await axios.get(
        `http://localhost:8080/user/reason/${userId}`
      );
      setUserInfo({ ...response.data, status: "Active" });

      // Redirect to the ManageUser page
      navigate("/ManageUser"); // Use navigate for redirection
    } catch (error) {
      console.error("Error unbanning user:", error);
      toast.error("Failed to unban user");
    }
  };

  return (
    <div className={style.body}>
      <div className="px-3 bg-white col-lg-12">
        <div
          className="container-fluid card-container "
          style={{ marginLeft: 150, width: 999 }}
        >
          <div
            className="row g-15 my-3 "
            style={{ marginLeft: 113, width: "80%" }}
          >
            <div className={style.Pg2buttons}>
              <Link to="/ManageUser">
                <button
                  style={{
                    borderRadius: "10px",
                    backgroundColor: "#2c73eb",
                    width: "90px",
                    height: "42px",
                    color: "white",
                    marginLeft: "20px",
                    background: "green",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    padding: "0",
                    border: "none",
                  }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  >
                    <path d="M19 12H6M12 5l-7 7 7 7" />
                  </svg>
                </button>
              </Link>

              <Popup
                trigger={
                  <button
                    className={style.addBtn}
                    style={{ background: "#FFB90A", border: "none" }}
                  >
                    {" "}
                    Unban User{" "}
                  </button>
                }
                modal
                nested
              >
                {(close) => (
                  <form
                    id="thisIsASampleForm"
                    onSubmit={handleUnbanUser}
                    style={{ height: "140px" }}
                  >
                    <div className="confirmation-modal1">
                      <p style={{ textAlign: "center", marginTop: "30px" }}>
                        Are you sure, you want to activate this user email?
                      </p>
                      <div
                        className={style.ubutton}
                        style={{ display: "flex", marginLeft: "24px" }}
                      >
                        <button
                          style={{ marginLeft: "9vh", background: "green" }}
                          type="submit"
                        >
                          <p>Yes</p>
                        </button>
                        <button
                          style={{ marginLeft: "35vh", background: "#FFB90A" }}
                          onClick={() => close()}
                        >
                          <p>No</p>
                        </button>
                      </div>
                    </div>
                  </form>
                )}
              </Popup>
            </div>
          </div>
          <div className={style.ManageUsers}>
            <div className={style.MuserInfo} style={{ marginLeft: "260px" }}>
              <div className={style.userInfoItem}>
                <h4>User Name</h4>
                <p className={style.userName} style={{ marginLeft: "20px" }}>
                  {userInfo && userInfo.name}
                </p>
              </div>
              <div className={style.userInfoItem}>
                <h4 style={{ marginLeft: "50px" }}>Email</h4>
                <p className={style.email}>{userInfo && userInfo.email}</p>
              </div>
              <div className={style.userInfoItem}>
                <h4>Account Status</h4>
                <p
                  className={style.accountStatus}
                  style={{
                    color:
                      userInfo && userInfo.status === "Active"
                        ? "#008000"
                        : "#FF0000",
                    fontWeight: "bold",
                    marginLeft: "40px",
                  }}
                >
                  {userInfo && userInfo.status}
                </p>
              </div>
            </div>
          </div>
          <div className={style.box}>
            <div className="Reason">
              <h4 style={{ marginTop: 20, textAlign: "center" }}>
                Reason for Banning:
              </h4>
              <p
                className="Astatus"
                style={{
                  color:
                    userInfo && userInfo.reasonForBanning === "Active"
                      ? "green"
                      : "red",
                  marginLeft: "20px",
                }}
              >
                {userInfo && userInfo.reasonForBanning}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UnbanUser;
