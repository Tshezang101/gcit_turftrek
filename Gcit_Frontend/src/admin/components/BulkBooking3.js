import React from 'react'


import 'react-calendar/dist/Calendar.css';
import 'bootstrap-icons/font/bootstrap-icons.css';
import style from "../css/page.module.css"
// import { Button } from 'bootstrap';
// import { useState } from 'react';

import Popup from 'reactjs-popup';
// import 'reactjs-popup/dist/index.css';
import { Link } from 'react-router-dom';



function BBookingtt({ Toggle }) {


    return (
        <div className={style.body}>
            <div className='px-3 bg-white col-lg-12'>
                <div className='container-fluid card-container '>
                    <div className='row g-15 my-3 ' style={{ marginLeft: '100px', width: '1044.766 px' }}>
                        <div className={style.Pg2buttons}>
                            <Link to='/Bbooking'> <button style={{
                                borderRadius: '10px',
                                backgroundColor: '#2c73eb',
                                width: '115px',
                                height: '42px',
                                color: 'white'
                            }}>Back</button></Link>
                            {/* <button className={style.addBtn}>Action</button> */}

                            <ul className="navbar-nav ms-auto mt-2 mt-lg-0">
                                <li className="nav-item dropdown">
                                    <button className={style.addBtn} to="#" id="dropdownId" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Action </button>   <div className="dropdown-menu" aria-labelledby="dropdownId" >
                                        {/* <Link className="dropdown-item" to="#">Profile</Link> */}

                                        <Popup
                                            trigger={<Link className="dropdown-item" to="#" >Payment</Link>}
                                            modal
                                            nested
                                            contentStyle={{
                                                height: '320px', display: 'inline-flex', width: '650px',
                                                padding: '20px 20px 20px 5px',
                                                flexDirection: 'column',

                                                gap: '68.722px',
                                                background: 'var(--dark-text-ffffff, #fff)'
                                            }}
                                        >
                                            {close => (
                                                <div >
                                                    <form
                                                        id="thisIsASampleForm"
                                                        style={{ height: '766px', marginLeft: '100px' }}
                                                        onSubmit={(e) => {
                                                            e.preventDefault();
                                                            close(); // Close the first popup
                                                            // Open the second popup
                                                        }}
                                                    >
                                                        <button
                                                            style={{
                                                                height: '20px',
                                                                width: '20px',
                                                                marginLeft: '450px',
                                                                alignItems: 'center',
                                                                paddingBottom: '25px',
                                                                paddingRight: '15px',
                                                            }}
                                                            id="close-btn"
                                                            onClick={() => close()}
                                                        >
                                                            &times;
                                                        </button>

                                                        <h5>Payment Details</h5>
                                                        <div className={style.formGroup}>
                                                            <label htmlFor="PayType">Payment Type</label>
                                                            <input type="text" name='PayType' />
                                                        </div>
                                                        <div className={style.formGroup}>
                                                            <label htmlFor="JournalNumber">Journal Number</label>
                                                            <input type="number" name="JournalNumber" placeholder="" />
                                                        </div>



                                                        <div className={style.ubutton}>
                                                            <button type="submit">
                                                                <p> Update </p>
                                                            </button>
                                                            <button style={{ marginLeft: '18vh' }} onClick={() => close()}>
                                                                <p> Cancel</p>
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            )}
                                        </Popup>

                                        <Popup
                                            trigger={<Link className="dropdown-item" to="#" >Cancel Booking</Link>}
                                            modal
                                            nested
                                            contentStyle={{
                                                height: '300px', display: 'inline-flex', width: '650px',
                                                padding: '20px 20px 20px 5px',
                                                flexDirection: 'column',

                                                gap: '68.722px',
                                                background: 'var(--dark-text-ffffff, #fff)'
                                            }}
                                        >
                                            {close => (
                                                <div >
                                                    <form
                                                        id="thisIsASampleForm"
                                                        style={{ height: '766px', marginLeft: '100px' }}
                                                        onSubmit={(e) => {
                                                            e.preventDefault();
                                                            close(); // Close the first popup
                                                            // Open the second popup
                                                        }}
                                                    >
                                                        <button
                                                            style={{
                                                                height: '20px',
                                                                width: '20px',
                                                                marginLeft: '450px',
                                                                alignItems: 'center',
                                                                paddingBottom: '25px',
                                                                paddingRight: '15px',
                                                            }}
                                                            id="close-btn"
                                                            onClick={() => close()}
                                                        >
                                                            &times;
                                                        </button>
                                                        <h1>Cancel Booking?</h1>

                                                        <h6> If you cancel the ground you won’t be able to recover it. Do you want to cancel the ground  ?</h6>


                                                        <div className={style.ubutton} style={{ marginTop: '2vh' }}>
                                                            <button type="submit">
                                                                <p> Confirm </p>
                                                            </button>
                                                            <button style={{ marginLeft: '18vh' }} onClick={() => close()}>
                                                                <p> Cancel</p>
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            )}
                                        </Popup>
                                    </div>
                                </li>
                            </ul>


                        </div>




                    </div>
                </div>

            </div>
            <div className={style.BulkContent}>

                <div className={style.UserInfo}>

                    <h4>Payment Information</h4>

                    <div>
                        <h6>Financial Status</h6>
                        <p className='FStatus'></p>
                    </div>
                    <div>
                        <h6>Amount</h6>
                        <p className='Amount'></p>
                    </div>
                    <div >
                        <h6>Ticket Status</h6>
                        <p className='TicketStatus'></p>
                    </div>
                    <div >
                        <h6>Payment Date</h6>
                        <p className='PaymentDate'></p>
                    </div>
                    <div >
                        <h6>Journal Number</h6>
                        <p className='JournalNo'></p>
                    </div>
                    <div >
                        <h6>User Bank Account</h6>
                        <p className='UBA'></p>
                    </div>
                </div>


            </div>
        </div >
    );
}
// function Action() {
//     return (
//         <Link to='/BBooking3'>    <button style={{ backgroundColor: 'rgba(44, 115, 235, 1)', borderRadius: 6, color: 'white' }}>
//             view
//         </button>
//         </Link>
//     );
// }



export default BBookingtt;