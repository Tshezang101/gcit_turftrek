import React, { useState, useEffect } from "react";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import axios from "axios";
import Sidebar from "./Sidebar"; // Adjust the path as needed

const Home = () => {
  const [isGcitHovered, setIsGcitHovered] = useState(false);
  const [isNonHovered, setIsNonHovered] = useState(false);
  const [isUserHovered, setIsUserHovered] = useState(false);
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [availableSlots, setAvailableSlots] = useState([]);
  const [userStats, setUserStats] = useState({
    totalUsers: 0,
    gcitUsersCount: 0,
    nonGcitUsersCount: 0,
  });

  const fetchUserStats = async () => {
    try {
      const response = await axios.get("http://localhost:8080/usercount");
      setUserStats(response.data);
    } catch (error) {
      console.error("Error Response:", error.response);
    }
  };

  useEffect(() => {
    fetchAvailableSlots(formatDateForCalendar(selectedDate));
    fetchUserStats(); // Fetch user stats when component mounts
  }, [selectedDate]);

  const formatDateForCalendar = (date) => {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0");
    const day = String(date.getDate()).padStart(2, "0");
    return `${year}-${month}-${day}`;
  };

  const fetchAvailableSlots = async (date) => {
    try {
      const response = await axios.get(
        `http://localhost:8080/availability/date/${date}`
      );
      const slots = response.data.availability.timeSlotsWithStatus;
      setAvailableSlots(slots);
    } catch (error) {
      console.error("Error fetching available slots:", error);
    }
  };

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  const isTwoWeeksAhead = (date) => {
    const today = new Date();
    const twoWeeksAhead = new Date();
    twoWeeksAhead.setDate(today.getDate() + 14); // Add 14 days

    return date > twoWeeksAhead;
  };

  return (
    <div style={{ display: "flex" }}>
      <Sidebar />
      <div style={{ marginLeft: "240px", padding: "20px", width: "100%" }}>
        <div style={{ padding: "20px" }}>
          <div
            style={{
              position: "fixed",
              top: 0,
              left: 240,
              right: 0,
              height: "90px",
              backgroundColor: "white",
              zIndex: 1000,
              borderBottom: "2px solid green",
              padding: "10px 20px",
              display: "flex",
              alignItems: "center",
            }}
          >
            <h1
              style={{
                color: "#052240",
                fontWeight: "bold",
                fontSize: "22px",
                fontWeight: "bold",
                fontSize: "30px",
                textAlign: "justify",
                marginTop: "3%",
                marginLeft: "1%",
              }}
            >
              Dashboard
            </h1>
          </div>
          <div
            style={{
              marginTop: "80px",
              display: "flex",
              marginRight: "10px",
              justifyContent: "space-around",
            }}
          >
            <div
              style={{
                transition: "transform 0.3s",
                width: "200px",
                height: "100px",
                transform: isGcitHovered ? "scale(1.2)" : "scale(1)",
                background: "#5DCC75",
                padding: "40px",
                borderRadius: "10px",
                display: "flex",
                marginLeft: "5px",
                marginRight: "130px",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                cursor: "pointer",
              }}
              onMouseEnter={() => setIsGcitHovered(true)}
              onMouseLeave={() => setIsGcitHovered(false)}
            >
              <p style={{ color: "#052240", fontSize: "16px" }}>GCIT Users</p>
              <h3 style={{ fontSize: "24px" }}>{userStats.gcitUsersCount}</h3>
            </div>
            <div
              style={{
                transition: "transform 0.3s",
                width: "200px",
                height: "100px",
                transform: isNonHovered ? "scale(1.2)" : "scale(1)",
                background: "#5DCC75",
                padding: "20px",
                borderRadius: "10px",
                display: "flex",
                marginRight: "130px",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                cursor: "pointer",
              }}
              onMouseEnter={() => setIsNonHovered(true)}
              onMouseLeave={() => setIsNonHovered(false)}
            >
              <p style={{ color: "#052240", fontSize: "16px" }}>
                NonGCIT Users
              </p>
              <h3 style={{ fontSize: "24px" }}>
                {userStats.nonGcitUsersCount}
              </h3>
            </div>
            <div
              style={{
                transition: "transform 0.3s",
                width: "200px",
                height: "100px",
                transform: isUserHovered ? "scale(1.2)" : "scale(1)",
                background: "#5DCC75",
                padding: "20px",
                borderRadius: "10px",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                cursor: "pointer",
              }}
              onMouseEnter={() => setIsUserHovered(true)}
              onMouseLeave={() => setIsUserHovered(false)}
            >
              <p style={{ color: "#052240", fontSize: "16px" }}>Total Users</p>
              <h3 style={{ fontSize: "24px" }}>{userStats.totalUsers}</h3>
            </div>
          </div>
        </div>
        <div style={{ display: "flex", marginTop: "80px" }}>
          <div
            style={{ marginRight: "20px", marginLeft: "40px", color: "green" }}
          >
            {/* <Calendar onChange={handleDateChange} value={selectedDate}  /> */}
            <Calendar
              onChange={handleDateChange}
              value={selectedDate}
              minDate={new Date()} // Prevent selecting past dates
            />
          </div>

          <div
            style={{
              overflowY: "auto",
              maxHeight: "400px",
              width: "100%",
              marginLeft: "110px",
              marginTop: "-40px",
            }}
          >
            <h3
              style={{
                color: "green",
                marginBottom: "20px",
                textAlign: "center",
                marginLeft: "-120px",
              }}
            >
              Availability:
            </h3>
            <div
              style={{
                display: "grid",
                gridTemplateColumns: "repeat(2, 1fr)",
                gridGap: "10px",
              }}
            >
              {isTwoWeeksAhead(selectedDate) ? (
                <p style={{ color: "red" }}>
                  No available slots posted for the dates that are two weeks
                </p>
              ) : availableSlots.length > 0 ? (
                availableSlots.map((slot, index) => (
                  <div key={index} style={{ marginBottom: "10px" }}>
                    <span>{slot.time}</span>
                    <span
                      style={{
                        marginLeft: "4px",
                        color: slot.status === "Booked" ? "red" : "green",
                      }}
                    >
                      {slot.status}
                    </span>
                  </div>
                ))
              ) : (
                <p style={{ color: "red" }}>
                  No available slots for selected date
                </p>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
