import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { FaTrash } from "react-icons/fa";

function BookingG({ navigate, post }) {
  const [bookings, setBookings] = useState([]);
  const [selectedBooking, setSelectedBooking] = useState([]);

  useEffect(() => {
    // Fetch bookings when the component mounts
    fetchBookings();
  }, []);

  const fetchBookings = async () => {
    try {
      const response = await fetch("http://localhost:8080/bookings/insider");
      if (!response.ok) {
        throw new Error("Failed to fetch bookings");
      }

      const data = await response.json();
      console.log("Fetched bookings:", data); // Log the response data
      setBookings(data);
    } catch (error) {
      console.error("Error fetching bookings:", error);
      toast.error("Failed to fetch the bookings");
    }
  };

  const handleDelete = async () => {
    try {
      const response = await fetch(
        `http://localhost:8080/bookings/${selectedBooking.bookingId}`,
        {
          method: "DELETE",
        }
      );
      if (!response.ok) {
        throw new Error("Failed to delete booking");
      }
      setBookings(
        bookings.filter(
          (booking) => booking.bookingId !== selectedBooking.bookingId
        )
      );
      toast.success("Booking deleted successfully");
    } catch (error) {
      console.error("Error deleting booking:", error);
      toast.error(error.message);
    }
  };

  const confirmDelete = (booking) => {
    setSelectedBooking(booking);
    if (window.confirm("Are you sure you want to delete this booking?")) {
      handleDelete();
    }
  };

  return (
    <div className="body">
      <div className="px-3 bg-white col-lg-12">
        <div className="container-fluid card-container">
          <div className="row g-15 my-3">
            <div
              className="fixed-header"
              style={{
                position: "fixed",
                top: 0,
                left: 240,
                right: 0,
                height: "90px",
                backgroundColor: "white",
                zIndex: 1000,
                borderBottom: "2px solid green",
                padding: "10px 20px",
                display: "flex",
                alignItems: "center",
              }}
            >
              <p
                className={`fs-10`}
                style={{
                  color: "#052240",
                  fontWeight: "bold",
                  fontSize: "24px",
                  fontWeight: "bold",
                  fontSize: "30px",
                  textAlign: "justify",
                  marginTop: "3%",
                  marginLeft: "1%",
                }}
              >
                Gcit Booking List
              </p>
            </div>
            <div
              className="px-3 bg-white col-lg-12"
              style={{ paddingTop: "60px" }}
            >
              <div className="container-fluid card-container">
                <div className="row g-15 my-3" style={{ marginLeft: 2 }}>
                  <table
                    style={{
                      width: "100%",
                      marginRight: "-40px",
                      backgroundColor: "white",
                      borderRadius: "1px",
                    }}
                  >
                    <thead>
                      <tr
                        style={{
                          position: "sticky",
                          top: 0,
                          backgroundColor: "white",
                          color: "black",
                          zIndex: 1,
                          borderBottom: "1px solid green",
                        }}
                      >
                        <th style={headerStyle}>Name</th>
                        <th style={headerStyle}>Email</th>
                        <th style={headerStyle}>Contact</th>
                        <th style={headerStyle}>Date</th>
                        <th style={headerStyle}>Time</th>
                        <th style={headerStyle}>Duration</th>
                        <th style={headerStyle}>Status</th>
                        <th style={headerStyle}> </th>
                      </tr>
                    </thead>
                    <tbody style={{ backgroundColor: "white" }}>
                      {bookings.map((booking, index) => (
                        <tr key={booking.id} style={rowStyle}>
                          <td style={cellStyle}>{booking.userId.name}</td>
                          <td style={cellStyle}>{booking.userId.email}</td>
                          <td style={cellStyle}>{booking.contact}</td>
                          <td style={cellStyle}>{booking.date.slice(0, 10)}</td>
                          <td style={cellStyle}>{booking.time}</td>
                          <td style={cellStyle}>{booking.duration}</td>

                          <td
                            style={{ color: "green", backgroundColor: "white" }}
                          >
                            Booked
                          </td>
                          <td>
                            <FaTrash
                              style={{
                                cursor: "pointer",
                                backgroundColor: "white",
                                color: "#052240",
                              }}
                              onClick={() => confirmDelete(booking)}
                            />
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default BookingG;
const headerStyle = {
  position: "sticky",
  top: "90px",
  left: "0",
  right: "0",
  padding: "6px",
  textAlign: "center",
  backgroundColor: "white",
  fontWeight: "bold",
  zIndex: 1,
  height: "55px",
};

const cellStyle = {
  padding: "18px",
  textAlign: "center",
  border: "1px",
  backgroundColor: "white",
};

const rowStyle = {
  backgroundColor: "white",
  borderBottom: "1px solid #ddd",
  cursor: "pointer",
  transition: "background-color 0.3s",
};
