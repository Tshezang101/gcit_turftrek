import React, { useState } from "react";
import logo from "../image/logo.png";
import { Link, useNavigate, useLocation } from "react-router-dom";
import "bootstrap-icons/font/bootstrap-icons.css";
import { useDispatch } from "react-redux";

function Sidebar() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();

  const [activeLink, setActiveLink] = useState(location.pathname);

  const handleLinkClick = (link) => {
    setActiveLink(link);
  };

  const linkHoverStyles = (link) => ({
    backgroundColor: activeLink === link ? "white" : "#FFB90A",
    color: activeLink === link ? "black" : "white",
    transition: "background-color 0.3s, color 0.3s",
    borderRadius: "15px",
  });

  const linkHover = (link) => ({
    onMouseEnter: (e) => {
      if (activeLink !== link) {
        e.currentTarget.style.backgroundColor = "white";
        e.currentTarget.style.color = "black";
      }
    },
    onMouseLeave: (e) => {
      if (activeLink !== link) {
        e.currentTarget.style.backgroundColor = "#FFB90A";
        e.currentTarget.style.color = "white";
      }
    },
  });

  const logout = () => {
    const confirmLogout = window.confirm("Are you sure you want to logout?");
    if (confirmLogout) {
      dispatch({
        type: "LOGOUT",
        payload: null,
      });
      window.localStorage.removeItem("auth");
      window.localStorage.removeItem("token"); // Remove the token from local storage
      navigate("/"); // Redirect to the AdminLogin page
    }
  };

  return (
    <div style={styles.sidebar}>
      <div className="m-1 rounded admin-navbar" style={styles.logoArea}>
        <div className="navbar-logo d-flex align-items-center">
          <div className="img-container">
            <img src={logo} alt="GCIT Logo" style={{ width: 59, height: 60 }} />
          </div>
          <div className="img-text">
            <h3
              className="text-dark fw-bold"
              style={{
                fontFamily: "Inknut Antiqua",
                fontSize: "20px",
                textAlign: "justify",
              }}
            >
              GCIT TURFTREK
            </h3>
          </div>
        </div>
      </div>
      <div className="list-group list-group-flush">
        {[
          { to: "/Dashboard", text: "Dashboard", icon: "bi bi-grid-fill" },
          {
            to: "/Booking",
            text: "GCIT Bookings",
            icon: "bi bi-file-text-fill",
          },
          {
            to: "/Booking2",
            text: "Non-GCIT Bookings",
            icon: "bi bi-file-text-fill",
          },
          {
            to: "/BBooking",
            text: "Bulk Booking",
            icon: "bi bi-file-text-fill",
          },
          {
            to: "/ManageUser",
            text: "Manage User",
            icon: "bi bi-person-fill-gear",
          },
          { to: "/setting", text: "Setting", icon: "bi bi-gear-fill" },
        ].map((link, index) => (
          <Link
            key={index}
            className="text-white list-group-item py-2 d-flex"
            style={{
              ...linkHoverStyles(link.to),
              marginTop: 10,
              marginLeft: 5,
              marginRight: 5,
            }}
            id="nav-links"
            to={link.to}
            onClick={() => handleLinkClick(link.to)}
            {...linkHover(link.to)}
          >
            <i
              className={`${link.icon} fs-5 me-3`}
              style={{ color: "black" }}
            ></i>
            <span style={{ color: "black", fontWeight: "bold" }}>
              {link.text}
            </span>
          </Link>
        ))}
        <div
          className="text-white list-group-item py-2 d-flex"
          style={{
            marginTop: 170,
            marginLeft: 5,
            marginRight: 5,
            cursor: "pointer",
            ...linkHoverStyles("/logout"),
          }}
          id="nav-links"
          onClick={() => {
            handleLinkClick("/logout");
            logout();
          }}
          {...linkHover("/logout")}
        >
          <i className="bi bi-power fs-5 me-3" style={{ color: "black" }}></i>
          <span style={{ color: "black", fontWeight: "bold" }}>Logout</span>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;

const styles = {
  sidebar: {
    backgroundColor: "#FFB90A",
    width: "240px",
    position: "fixed",
    left: "0",
    top: "0",
    height: "100%",
    zIndex: "1",
    overflowX: "hidden",
    paddingTop: "2px",
  },
  logoArea: {
    backgroundColor: "white",
    height: "85px",
    padding: "10px",
    paddingTop: "15px",
    borderRadius: "30px",
    boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.2)",
  },
};
