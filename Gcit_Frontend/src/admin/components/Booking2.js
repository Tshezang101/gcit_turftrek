import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { FaTrash } from "react-icons/fa";

function Booking({ navigate, post }) {
  const [bookings, setBookings] = useState([]);
  const [selectedBooking, setSelectedBooking] = useState(null);
  const [popupVisible, setPopupVisible] = useState(false);
  const [updatedBooking, setUpdatedBooking] = useState(null);

  useEffect(() => {
    // Fetch bookings when the component mounts
    fetchBookings();
  }, []);

  const fetchBookings = async () => {
    try {
      const response = await fetch("http://localhost:8080/bookings/outsider");
      if (!response.ok) {
        throw new Error("Failed to fetch bookings");
      }
      const data = await response.json();
      console.log(data);
      setBookings(data);
    } catch (error) {
      console.error("Error fetching bookings:", error);
      toast.error("Failed to fetch the bookings");
    }
  };

  const handleBookingClick = (booking) => {
    setSelectedBooking(booking);
    setPopupVisible(true);
  };

  const handleConfirm = async () => {
    try {
      const updatedBooking = {
        ...selectedBooking,
        paymentStatus: "paid",
        status: "Booked",
      };
      const response = await fetch(
        `http://localhost:8080/bookings/${selectedBooking.bookingId}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(updatedBooking),
        }
      );
      if (!response.ok) {
        throw new Error("Failed to confirm booking");
      }
      setUpdatedBooking(updatedBooking);
      setPopupVisible(false);
      // Update local state with the updated booking
      setBookings(
        bookings.map((booking) =>
          booking.bookingId === selectedBooking.bookingId
            ? updatedBooking
            : booking
        )
      );
      toast.success("Booking confirmed successfully");
    } catch (error) {
      console.error("Error confirming booking:", error);
      toast.error("Failed to confirm booking");
    }
  };

  const handleReject = async () => {
    try {
      const updatedBooking = {
        ...selectedBooking,
        paymentStatus: "reject",
        status: "Rejected",
      };
      const response = await fetch(
        `http://localhost:8080/bookings/${selectedBooking.bookingId}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(updatedBooking),
        }
      );
      if (!response.ok) {
        throw new Error("Failed to reject booking");
      }
      setUpdatedBooking(updatedBooking);
      setPopupVisible(false);
      // Update local state with the updated booking
      setBookings(
        bookings.map((booking) =>
          booking.bookingId === selectedBooking.bookingId
            ? updatedBooking
            : booking
        )
      );
      toast.success("Booking rejected successfully");
    } catch (error) {
      console.error("Error rejecting booking:", error);
      toast.error("Failed to reject booking");
    }
  };

  const handleCancel = async () => {
    try {
      const updatedBooking = {
        ...selectedBooking,
        paymentStatus: "cancel",
        status: "Cancelled",
      };
      const response = await fetch(
        `http://localhost:8080/bookings/cancel/${selectedBooking.bookingId}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(updatedBooking),
        }
      );
      if (!response.ok) {
        throw new Error("Failed to cancel booking");
      }
      setUpdatedBooking(updatedBooking);
      // Update local state with the updated booking
      setBookings(
        bookings.map((booking) =>
          booking.bookingId === selectedBooking.bookingId
            ? updatedBooking
            : booking
        )
      );
      setPopupVisible(false);
      toast.success("Booking cancelled successfully");
    } catch (error) {
      console.error("Error cancelling booking:", error);
      toast.error(error.message);
    }
  };

  const handleDelete = async () => {
    try {
      const response = await fetch(
        `http://localhost:8080/bookings/${selectedBooking.bookingId}`,
        {
          method: "DELETE",
        }
      );
      if (!response.ok) {
        throw new Error("Failed to delete booking");
      }
      setBookings(
        bookings.filter(
          (booking) => booking.bookingId !== selectedBooking.bookingId
        )
      );
      setPopupVisible(false);
      toast.success("Booking deleted successfully");
    } catch (error) {
      console.error("Error deleting booking:", error);
      toast.error(error.message);
    }
  };

  const confirmDelete = () => {
    if (window.confirm("Are you sure you want to delete this booking?")) {
      handleDelete();
    }
  };

  const getClassByStatus = (status) => {
    switch (status) {
      case "Pending":
        return "pending";
      case "Booked":
        return "booked";
      case "Rejected":
      case "Cancelled":
        return "cancelled";
      default:
        return "";
    }
  };

  return (
    <div className="body">
      <style>{styles}</style>

      <div className="px-3 bg-white col-lg-12">
        <div className="container-fluid card-container">
          <div
            className="row g-12 my-0"
            style={{
              position: "fixed",
              top: 0,
              left: 252,
              right: 0,
              height: "90px",
              zIndex: 1000,
              // backgroundColor: "white",
              borderBottom: "2px solid green",
              padding: "10px 20px",
              display: "flex",
              alignItems: "center",
            }}
          >
            <p
              className={`fs-10`}
              style={{
                color: "#052240",
                fontWeight: "bold",
                fontSize: "24px",
                fontWeight: "bold",
                fontSize: "30px",
                textAlign: "justify",
                marginTop: "2%",
                marginLeft: "1%",
              }}
            >
              Non-GCIT Booking List
            </p>
          </div>
          <div
            className="table"
            style={{
              marginTop: "90px",
              marginLeft: "13px",
              // marginRight: "30px",
              width: "100%",
            }}
          >
            <table
              style={{
                width: "100%",
                marginRight: "-30px",
                backgroundColor: "white",
                borderRadius: "1px",
              }}
            >
              <thead>
                <tr
                  style={{
                    position: "sticky",
                    top: 0,

                    backgroundColor: "white",
                    zIndex: 1,
                  }}
                >
                  <th style={headerStyle}>Name</th>
                  <th style={headerStyle}>Email</th>
                  <th style={headerStyle}>Contact</th>
                  <th style={headerStyle}>Bank Name</th>
                  <th style={headerStyle}>Date</th>
                  <th style={headerStyle}>Time</th>
                  <th style={headerStyle}>Journal Number</th>
                  <th style={headerStyle}>Duration</th>
                  <th style={headerStyle}>Payment Status</th>
                  <th style={headerStyle}>Status</th>
                  <th style={headerStyle}></th>
                </tr>
              </thead>
              <tbody>
                {bookings.map((booking, index) => (
                  <tr
                    key={booking.id}
                    onClick={() => handleBookingClick(booking)}
                    style={rowStyle}
                    className={getClassByStatus(booking.status)}
                  >
                    <td style={cellStyle}>{booking.userId.name}</td>
                    <td style={cellStyle}>{booking.userId.email}</td>
                    <td style={cellStyle}>{booking.contact}</td>
                    <td style={cellStyle}>{booking.bankName}</td>
                    <td style={cellStyle}>{booking.date.slice(0, 10)}</td>
                    <td style={cellStyle}>{booking.time}</td>
                    <td style={cellStyle}>{booking.journalNumber}</td>
                    <td style={cellStyle}>{booking.duration}</td>

                    <td style={cellStyle}>{booking.paymentStatus}</td>
                    <td
                      style={{
                        color:
                          booking.status === "Pending"
                            ? "blue"
                            : booking.status === "Booked"
                            ? "green"
                            : "red",
                      }}
                    >
                      {booking.status}
                    </td>
                    <td style={cellStyle}>
                      <FaTrash
                        style={{ cursor: "pointer", color: "#052240" }}
                        onClick={confirmDelete}
                      />
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>

      {popupVisible &&
        selectedBooking &&
        selectedBooking.status === "Pending" && (
          <div>
            <div className="popup-overlay"></div>
            <div className="popup">
              <div className="popup-content">
                <span
                  className="close"
                  onClick={() => setPopupVisible(false)}
                  style={{
                    cursor: "pointer",
                    height: "30px",
                    width: "30px",
                    marginLeft: "350px",
                  }}
                >
                  &times;
                </span>
                <h3>Booking Details</h3>
                <p>
                  Name: <b>{selectedBooking.userId.name}</b>
                </p>
                <p>
                  Email: <b>{selectedBooking.userId.email}</b>
                </p>
                <p>
                  Contact: <b>{selectedBooking.contact}</b>
                </p>
                <p>
                  Bank Name: <b>{selectedBooking.bankName}</b>
                </p>
                <p>
                  Date: <b>{selectedBooking.date.slice(0, 10)}</b>
                </p>
                <p>
                  Time: <b>{selectedBooking.time}</b>
                </p>
                <p>
                  Journal Number: <b>{selectedBooking.journalNumber}</b>
                </p>
                <p>
                  Duration: <b>{selectedBooking.duration}</b>
                </p>

                <p>
                  Payment Status: <b>{selectedBooking.paymentStatus}</b>
                </p>
                <p
                  style={{
                    color:
                      selectedBooking.status === "Pending"
                        ? "blue"
                        : selectedBooking.status === "Booked"
                        ? "green"
                        : "red",
                    padding: "6px",
                    textAlign: "center",
                    border: "1px solid #ddd",
                  }}
                >
                  <b>{selectedBooking.status}</b>
                </p>
                <div className="actions" style={{ marginBottom: "25px" }}>
                  <button
                    onClick={handleConfirm}
                    style={{
                      marginRight: "10px",
                      backgroundColor: "green",
                      color: "white",
                      borderRadius: "5px",
                      padding: "5px 10px",
                      border: "none",
                      marginLeft: "30px",
                    }}
                  >
                    Confirm
                  </button>
                  <button
                    onClick={handleReject}
                    style={{
                      backgroundColor: "#FFB90A",
                      color: "white",
                      borderRadius: "5px",
                      padding: "5px 10px",
                      border: "none",
                      marginLeft: "190px",
                      width: "75px",
                    }}
                  >
                    Reject
                  </button>
                </div>
              </div>
            </div>
          </div>
        )}

      {popupVisible &&
        selectedBooking &&
        selectedBooking.status === "Booked" && (
          <div>
            <div className="popup-overlay"></div>
            <div className="popup">
              <div className="popup-content">
                <span
                  className="close"
                  onClick={() => setPopupVisible(false)}
                  style={{
                    cursor: "pointer",
                    height: "30px",
                    width: "30px",
                    marginLeft: "350px",
                  }}
                >
                  &times;
                </span>
                <h3>Booking Details</h3>
                <p>
                  Name: <b>{selectedBooking.userId.name}</b>
                </p>
                <p>
                  Email: <b>{selectedBooking.userId.email}</b>
                </p>
                <p>
                  Contact: <b>{selectedBooking.contact}</b>
                </p>
                <p>
                  Bank Name: <b>{selectedBooking.bankName}</b>
                </p>
                <p>
                  Date: <b>{selectedBooking.date.slice(0, 10)}</b>
                </p>
                <p>
                  Time: <b>{selectedBooking.time}</b>
                </p>
                <p>
                  Journal Number: <b>{selectedBooking.journalNumber}</b>
                </p>
                <p>
                  Duration: <b>{selectedBooking.duration}</b>
                </p>

                <p>
                  Payment Status: <b>{selectedBooking.paymentStatus}</b>
                </p>
                <p
                  style={{
                    color:
                      selectedBooking.status === "Pending"
                        ? "#052240"
                        : selectedBooking.status === "Booked"
                        ? "green"
                        : "red",
                  }}
                >
                  <b>{selectedBooking.status}</b>
                </p>
                <div className="actions" style={{ marginBottom: "25px" }}>
                  <button
                    onClick={handleCancel}
                    style={{
                      backgroundColor: "#FFB90A",
                      color: "white",
                      borderRadius: "5px",
                      padding: "5px 10px",
                    }}
                  >
                    Cancel Booking
                  </button>
                </div>
              </div>
            </div>
          </div>
        )}

      {popupVisible &&
        selectedBooking &&
        (selectedBooking.status === "Rejected" ||
          selectedBooking.status === "Cancelled") && (
          <div>
            <div className="popup-overlay"></div>
            <div className="popup">
              <div className="popup-content">
                <span
                  className="close"
                  onClick={() => setPopupVisible(false)}
                  style={{
                    cursor: "pointer",
                    height: "100px",
                    width: "100px",
                    marginLeft: "380px",
                  }}
                >
                  &times;
                </span>
                <h4
                  style={{
                    fontSize: "bold",
                    color: "black",
                    textAlign: "center",
                  }}
                >
                  Booking Details
                </h4>
                <div>
                  <p>
                    Name: <b>{selectedBooking.userId.name}</b>
                  </p>
                  <p>
                    Email: <b>{selectedBooking.userId.email}</b>
                  </p>
                  <p>
                    Contact: <b>{selectedBooking.contact}</b>
                  </p>
                  <p>
                    Bank Name: <b>{selectedBooking.bankName}</b>
                  </p>
                  <p>
                    Date: <b>{selectedBooking.date.slice(0, 10)}</b>
                  </p>
                  <p>
                    Time: <b>{selectedBooking.time}</b>
                  </p>
                  <p>
                    Journal Number: <b>{selectedBooking.journalNumber}</b>
                  </p>
                  <p>
                    Duration: <b>{selectedBooking.duration}</b>
                  </p>

                  <p>
                    Payment Status: <b>{selectedBooking.paymentStatus}</b>
                  </p>
                  <p
                    style={{
                      color:
                        selectedBooking.status === "Pending"
                          ? "#052240"
                          : selectedBooking.status === "Booked"
                          ? "green"
                          : "red",
                    }}
                  >
                    <b>{selectedBooking.status}</b>
                  </p>
                </div>
              </div>
            </div>
          </div>
        )}
    </div>
  );
}

export default Booking;
const headerStyle = {
  position: "sticky",
  top: "90px",
  left: "0",
  right: "0",
  padding: "6px",
  textAlign: "center",
  backgroundColor: "white",
  fontWeight: "bold",
  zIndex: 1,
  height: "60px",
};

const cellStyle = {
  padding: "6px",
  textAlign: "center",
  border: "1px",
  backgroundColor: "white",
};

const rowStyle = {
  backgroundColor: "white",
  borderBottom: "1px solid #ddd",
  cursor: "pointer",
  transition: "background-color 0.3s",
};

const styles = `
.popup-overlay {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5); /* Semi-transparent background */
    z-index: 999; /* Ensure the overlay is below the popup */
}

.popup {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 1000;
    display: flex;
    align-items: center;
    justify-content: center;
}

.popup-content {
    background-color: white;
    padding: 20px;
    border-radius: 10px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.3); /* Adding a shadow effect */
    width: 450px;
    overflow-y: auto; 
}

.popup-content::-webkit-scrollbar {
    width: 8px; /* Width of the scrollbar */
}

.popup-content::-webkit-scrollbar-thumb {
    background-color: rgba(0, 0, 0, 0.3); /* Color of the scrollbar thumb */
    border-radius: 4px; /* Border radius of the scrollbar thumb */
}

.popup-content::-webkit-scrollbar-track {
    background-color: transparent; /* Color of the scrollbar track */
}

.pending {
    color: blue;
}

.booked {
    color: green;
}

.cancelled {
    color: red; 
}
`;
