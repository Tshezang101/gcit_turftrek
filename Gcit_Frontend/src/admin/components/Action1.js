import React from 'react';
import { Link } from 'react-router-dom';

const Action = ({ userId, label }) => {
    return (
        <Link to={`/ManageUsers/${userId}`}>
            <button style={{ backgroundColor: 'rgba(44, 115, 235, 1)', borderRadius: 6, color: 'white' }}>
                {label}
            </button>
        </Link>
    );
};

export default Action;
