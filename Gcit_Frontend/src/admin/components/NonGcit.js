import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import Popup from "reactjs-popup";
import { Link } from "react-router-dom";
import { getBookingAPI, getfreeSlotsAPI, bookGround } from "../action/booking";
import { toast } from "react-toastify";

function Booking({ Toggle }) {
  const [bookings, setbookings] = useState([]);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");

  const [contact, setPhoneNumber] = useState("");
  const [bookingid, setbookingid] = useState("");

  return (
    <div className="body">
      <div className="px-3 bg-white col-lg-12">
        <div className="container-fluid card-container ">
          <div className="row g-15 my-3 " style={{ width: "1044.766 px" }}>
            <p
              style={{
                textAlign: "left",
                fontWeight: "bold",
                marginTop: "30px",
                fontSize: "30px",
              }}
            >
              Non-GCIT Booking List
            </p>
            <hr className="my-hr" style={{ borderTop: "2px solid green" }} />

            <table className="table caption-top bg-white rounded mt-2">
              <thead>
                <tr>
                  <th scope="col">TicketID</th>
                  <th scope="col">Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">Contact</th>
                  <th scope="col">Date</th>
                  <th scope="col">Time</th>
                  {/* <th scope="col">Journal Number</th>
                                    <th scope="col">Bank</th>
                                    <th scope="col">Status</th> */}
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td scope="col">101</td>
                  <td scope="col">Phuntsho</td>
                  <td scope="col">12220054.gcit@rub.edu.bt</td>
                  <td scope="col">17835360</td>
                  <td scope="col">10-04-2024</td>
                  <td scope="col">2-4 pm</td>
                  {/* <td scope="col">2-4 pm</td>
                                    <td scope="col">2-4 pm</td>
                                    <td scope="col">2-4 pm</td> */}
                </tr>
                {/* {
                                    bookings.map((booking, index) => (
                                        <tr key={booking.id}>
                                            <th scope="row">{index + 1}</th>
                                            <td>{booking.name}</td>
                                            <td>{booking.email}</td>
                                            <td>{booking.contact}</td>
                                            <td>{booking.date}</td>
                                            <td>{booking.time}</td>


                                            <td><Action id={booking.id}></Action></td>
                                        </tr>
                                    ))} */}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

function Action({ id }) {
  return (
    <Link to={`/Booking2/${id}`}>
      <button
        style={{
          backgroundColor: "rgba(44, 115, 235, 1)",
          borderRadius: 6,
          color: "white",
        }}
      >
        view
      </button>
    </Link>
  );
}

export default Booking;
