import React from "react";
import { Navigate, Outlet } from "react-router-dom";

const PublicRoute = ({ restricted }) => {
  const token = localStorage.getItem("token");

  return token && restricted ? <Navigate to="/Dashboard" /> : <Outlet />;
};

export default PublicRoute;
