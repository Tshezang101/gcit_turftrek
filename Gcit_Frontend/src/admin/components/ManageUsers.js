import React, { useEffect, useState } from "react";
import { useParams, useNavigate, Link } from "react-router-dom";
import axios from "axios";
import style from "../css/page.module.css";
import Popup from "reactjs-popup";
import { toast } from "react-toastify";

import "../css/page.module.css";
import "../css/setting.css";

const ManageUsers = () => {
  const [bookings, setBookings] = useState([]);
  const [userInfo, setUserInfo] = useState(null);
  const { userId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchUserInfo = async () => {
      try {
        const response = await axios.get(
          `http://localhost:8080/user/reason/${userId}`
        );
        setUserInfo(response.data);
      } catch (error) {
        console.error("Error fetching user data:", error);
        toast.error("Failed to fetch user data");
      }
    };

    const fetchBookings = async () => {
      try {
        const response = await axios.get(
          "http://localhost:8080/bookings/outsider"
        );
        setBookings(response.data);
      } catch (error) {
        console.error("Error fetching bookings:", error);
        toast.error("Failed to fetch bookings");
      }
    };

    fetchUserInfo();
    fetchBookings();
  }, [userId]);

  const handleBanUser = async (event, reason) => {
    event.preventDefault();
    try {
      const response = await axios.post(
        `http://localhost:8080/users/ban/${userId}`,
        {
          reason: "This user has been banned.",
        }
      );
      setUserInfo({
        ...userInfo,
        status: "Banned",
        reasonForBanning: response.data.reasonForBanning,
      });
      navigate("/ManageUser");
    } catch (error) {
      console.error("Error banning user:", error);
      toast.error("Failed to ban user");
    }
  };

  return (
    <div className={style.body}>
      <div className="px-3 bg-white col-lg-12">
        <div
          className="container-fluid card-container "
          style={{ marginLeft: 150, width: 999 }}
        >
          <div
            className="row g-15 my-3 "
            style={{ marginLeft: 100, width: "90%" }}
          >
            <div className={style.Pg2buttons}>
              <Link to="/ManageUser">
                <button
                  style={{
                    borderRadius: "10px",
                    backgroundColor: "#2c73eb",
                    width: "90px",
                    height: "42px",
                    color: "white",
                    marginLeft: "20px",
                    background: "green",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    padding: "0",
                    border: "none",
                  }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  >
                    <path d="M19 12H6M12 5l-7 7 7 7" />
                  </svg>
                </button>
              </Link>
              <Popup
                trigger={
                  <button
                    className={style.addBtn}
                    style={{ background: "#FFB90A", border: "none" }}
                  >
                    {" "}
                    Ban User{" "}
                  </button>
                }
                modal
                nested
                style={{ color: "green" }}
              >
                {(close) => (
                  <div className="confirmation-modal1">
                    <form
                      id="thisIsASampleForm"
                      action=""
                      style={{
                        marginLeft: "40px",
                        height: "430px",
                      }}
                      onSubmit={handleBanUser}
                    >
                      <div className="confirmation-modal1">
                        <h2 style={{ marginLeft: "150px", marginTop: "30px" }}>
                          Banning User
                        </h2>
                        <p>
                          Could you please provide the reason for your decision
                          to ban this account
                        </p>
                        <textarea
                          rows={10}
                          cols={72}
                          placeholder="The reason to ban this account is "
                        ></textarea>
                        <div className={style.ubutton}>
                          <button
                            style={{
                              marginLeft: "10vh",
                              background: "green",
                              border: "none",
                            }}
                            type="submit"
                          >
                            <p> Ban </p>
                          </button>
                          <button
                            style={{
                              marginLeft: "25vh",
                              background: "#FFB90A",
                            }}
                            onClick={() => close()}
                          >
                            <p> cancel</p>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                )}
              </Popup>
            </div>
          </div>
          <div className={style.ManageUsers}>
            <div className={style.MuserInfo} style={{ marginLeft: "260px" }}>
              <div className={style.userInfoItem}>
                <h4>User Name</h4>
                <p className={style.userName} style={{ marginLeft: "20px" }}>
                  {userInfo && userInfo.name}
                </p>
              </div>
              <div className={style.userInfoItem}>
                <h4 style={{ marginLeft: "50px" }}>Email</h4>
                <p className={style.email}>{userInfo && userInfo.email}</p>
              </div>
              <div className={style.userInfoItem}>
                <h4>Account Status</h4>
                <p
                  className={style.accountStatus}
                  style={{
                    color:
                      userInfo && userInfo.status === "Active"
                        ? "#008000"
                        : "#FF0000",
                    fontWeight: "bold",
                    marginLeft: "40px",
                  }}
                >
                  {userInfo && userInfo.status}
                </p>
              </div>
            </div>
            <div className={style.box}>
              <table
                style={{
                  width: "100%",
                  marginRight: "-40px",
                  backgroundColor: "white",
                  borderRadius: "1px",
                }}
              >
                <thead>
                  <tr
                    style={{
                      position: "sticky",
                      top: 0,
                      backgroundColor: "white",
                      zIndex: 1,
                      paddingBottom: "10px",
                    }}
                  >
                    <th style={headerStyle}>Contact</th>
                    <th style={headerStyle}>Date</th>
                    <th style={headerStyle}>Time</th>
                    <th style={headerStyle}>Payment Status</th>
                    <th style={headerStyle}>Status</th>
                  </tr>
                </thead>
                <tbody>
                  {bookings.map((booking, index) => (
                    <tr key={index} style={rowStyle}>
                      <td style={cellStyle}>{booking.contact}</td>
                      <td style={cellStyle}>{booking.date.slice(0, 10)}</td>
                      <td style={cellStyle}>{booking.time}</td>
                      <td style={cellStyle}>{booking.paymentStatus}</td>
                      <td style={cellStyle}>{booking.status}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ManageUsers;
const headerStyle = {
  position: "sticky",
  // top: "90px",
  left: "0",
  right: "0",
  padding: "6px",
  textAlign: "center",
  backgroundColor: "white",
  fontWeight: "bold",
  zIndex: 1,
  height: "60px",
  borderBottom: "1px solid green",
};

const cellStyle = {
  padding: "6px",
  textAlign: "center",
  border: "1px",
  paddingTop: "30px",
  backgroundColor: "white",
};

const rowStyle = {
  backgroundColor: "white",
  borderBottom: "1px solid #ddd",
  cursor: "pointer",
  transition: "background-color 0.3s",
};
