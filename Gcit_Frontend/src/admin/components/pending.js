import React, { useState } from 'react'
import { useEffect } from 'react';


import 'react-calendar/dist/Calendar.css';
import 'bootstrap-icons/font/bootstrap-icons.css';
import style from "../css/page.module.css"
import 'reactjs-popup/dist/index.css';
import { Link } from 'react-router-dom';
import { dueBookingsAPI } from "../action/booking"

function Pending({ Toggle }) {
    const [dueBookings, setDueBookings] = useState([]);
    useEffect(() => {
        const getBookings = async () => {
            try {
                const res = await dueBookingsAPI();
                setDueBookings(res.data.data);
                console.log(res.data.data)
            } catch (error) {
                console.log(error)
            }
        };

        getBookings();
    }, []);

    return (
        <div className={style.body}>
            <div className='px-3 bg-white col-lg-12'>
                <div className='container-fluid card-container '>
                    <div className='row g-15 my-3 ' style={{ marginLeft: 100, width: 1044.766 }}>
                        <h1 className={style.header}>Pending</h1>


                        <table class="table caption-top bg-white rounded mt-2">
                            <caption className='text-white fs-4'>Recent Orders</caption>
                            <thead>
                                <tr>

                                    <th scope="col">Customer Name</th>
                                    <th scope="col">Phone Number</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Status</th>

                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    dueBookings.map((booking, index) => (
                                        <tr key={booking.id}>
                                            <th scope="row">{index + 1}</th>
                                            <td>{booking.user_name}</td>
                                            <td>{booking.match_date}</td>
                                            <td>{booking.match_start_time}</td>
                                            <td>{booking.match_status}</td>
                                            <td><Action id={booking.id}></Action>
                                            </td>
                                        </tr>
                                    ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div >
    )
}

function Action({ id }) {
    return (
        <Link to={`/due/${id}`} > <button style={{ backgroundColor: 'rgba(44, 115, 235, 1)', borderRadius: 6, color: 'white' }}>
            view
        </button>
        </Link >
    )
}


export default Pending;