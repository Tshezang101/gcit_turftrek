import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "react-datepicker/dist/react-datepicker.css"; // Import the styles for react-datepicker
import "bootstrap-icons/font/bootstrap-icons.css";
import style from "../css/page.module.css";
import Popup from "reactjs-popup";
import "reactjs-popup/dist/index.css";
import { FaTrash } from "react-icons/fa";

function BulkBooking({ toggle }) {
  const [bulkbookings, setbulkBookings] = useState([]);
  const [selectedDates, setSelectedDates] = useState([]);
  const [startTime, setStartTime] = useState("");
  const [endTime, setEndTime] = useState("");
  const [newDate, setNewDate] = useState("");

  useEffect(() => {
    // Fetch bookings when the component mounts
    fetchBookings();
  }, []);

  const fetchBookings = async () => {
    try {
      const response = await fetch(
        "http://localhost:8080/availability/bulkbookings"
      );
      if (!response.ok) {
        throw new Error("Failed to fetch bookings");
      }

      const data = await response.json();
      console.log("Fetched bookings:", data); // Log the response data
      setbulkBookings(data);
    } catch (error) {
      console.error("Error fetching bookings:", error);
      toast.error("Failed to fetch the bookings");
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    // Validate form fields
    if (!startTime || !endTime || selectedDates.length === 0) {
      toast.error("Please fill out all fields");
      return;
    }

    const timeFormat = /^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/;

    if (!timeFormat.test(startTime) || !timeFormat.test(endTime)) {
      toast.error("Please enter the time in the 24-hour format (e.g., 14:00)");
      return;
    }

    try {
      console.log(
        JSON.stringify({
          selectedDates,
          startTime,
          endTime,
        })
      );
      const response = await fetch(
        "http://localhost:8080/availability/bulkbooking",
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            dates: selectedDates,
            startTime: startTime,
            endTime: endTime,
          }),
        }
      );

      if (!response.ok) {
        throw new Error(
          "Failed to submit the bulk booking! Please check the format"
        );
      }

      toast.success("Bulk Booking Success !");
      // Refresh bulk bookings after successful submission
      fetchBookings();
    } catch (error) {
      console.error("Error:", error);
      toast.error(
        "Booking failed! Please ensure your time format follows the 24-hour clock. For example, 2 PM should be represented as 14:00."
      );
    }
  };

  const handleDelete = async (date, time) => {
    try {
      const response = await fetch(
        `http://localhost:8080/availability/bulkbookings?date=${date}&time=${time}`,
        {
          method: "DELETE",
        }
      );
      if (!response.ok) {
        throw new Error("Failed to delete bulk booking");
      }
      fetchBookings(); // Refresh bulk bookings after successful deletion
      toast.success("Bulk booking deleted successfully");
    } catch (error) {
      console.error("Error deleting bulk booking:", error);
      toast.error(error.message);
    }
  };

  const confirmDelete = (date, time) => {
    if (window.confirm("Are you sure you want to delete this bulk booking?")) {
      handleDelete(date, time);
    }
  };

  const handleAddDate = () => {
    if (newDate && !selectedDates.includes(newDate)) {
      setSelectedDates([...selectedDates, newDate]);
      setNewDate("");
    }
  };

  const handleRemoveDate = (date) => {
    setSelectedDates(selectedDates.filter((d) => d !== date));
  };

  function getCurrentDate() {
    const date = new Date();
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const day = date.getDate().toString().padStart(2, "0");
    return `${year}-${month}-${day}`;
  }

  return (
    <div className={style.body}>
      <div className="px-3 bg-white col-lg-12">
        <div className="container-fluid card-container ">
          <div className="row g-15 my-3 align-items-center">
            <div className="row g-15 my-3">
              <div
                className="fixed-header"
                style={{
                  position: "fixed",
                  top: 0,
                  left: 240,
                  right: 0,
                  height: "90px",
                  backgroundColor: "white",
                  zIndex: 1000,
                  borderBottom: "2px solid green",
                  padding: "10px 20px",
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <p
                  className={`fs-10`}
                  style={{
                    color: "#052240",
                    fontWeight: "bold",
                    fontSize: "24px",
                    fontWeight: "bold",
                    fontSize: "30px",
                    textAlign: "justify",
                    marginTop: "3%",
                    marginLeft: "1%",
                  }}
                >
                  BulkBooking
                </p>
                <Popup
                  trigger={
                    <button
                      className={style.addBtn}
                      style={{
                        fontSize: "14px",
                        marginLeft: "700px",
                        marginBottom: "-0px",
                        width: "90px",
                        border: "none",
                      }}
                    >
                      Add new +{" "}
                    </button>
                  }
                  modal
                  nested
                  contentStyle={{
                    height: "450px",
                    display: "inline-flex",
                    width: "550px",
                    padding: "25px",
                    flexDirection: "column",
                    overflowY: "scroll",
                    gap: "68.722px",
                    background: "var(--dark-text-ffffff, #fff)",
                    boxShadow: "0 0 10px rgba(0, 0, 0, 0.3)",
                    borderRadius: "10px",
                  }}
                >
                  {(close) => (
                    <div>
                      <form
                        id="bulkBookingForm"
                        onSubmit={(e) => {
                          handleSubmit(e);
                          close();
                        }}
                      >
                        <div className="whole-form">
                          <button
                            style={{
                              height: "20px",
                              width: "20px",
                              marginLeft: "450px",
                              alignItems: "center",
                              paddingBottom: "25px",
                              paddingRight: "15px",
                            }}
                            id="close-btn"
                            onClick={() => close()}
                          >
                            &times;
                          </button>
                          <div>
                            <label htmlFor="startTime">
                              <b>Start Time :</b>
                            </label>
                            <input
                              type="text"
                              id="startTime"
                              name="startTime"
                              value={startTime}
                              onChange={(e) => setStartTime(e.target.value)}
                              placeholder="  ex. 14:00"
                              style={{
                                width: "200px", // Adjust this value as needed
                                borderRadius: "5px",
                                marginBottom: "10px",
                                padding: "5px",
                                marginLeft: "8px",
                              }}
                            />
                          </div>
                          <div>
                            <label htmlFor="endTime">
                              <b>End Time :</b>
                            </label>
                            <input
                              type="text"
                              id="endTime"
                              name="endTime"
                              value={endTime}
                              onChange={(e) => setEndTime(e.target.value)}
                              placeholder="  ex. 15:00"
                              style={{
                                width: "200px", // Adjust this value as needed
                                borderRadius: "5px",
                                marginBottom: "10px",
                                padding: "5px",
                                marginLeft: "10px",
                              }}
                            />
                          </div>

                          <div style={{ display: "flex" }}>
                            <label
                              htmlFor="edate"
                              style={{ width: "5%", marginRight: "20px" }}
                            >
                              <b>Dates:</b>
                            </label>
                            <div>
                              <input
                                type="date"
                                value={newDate}
                                min={getCurrentDate()}
                                placeholder="YYYY-MM-DD"
                                onChange={(e) => setNewDate(e.target.value)}
                                style={{
                                  width: "350px", // Adjust this value as needed
                                  borderRadius: "5px",
                                  marginBottom: "10px",
                                  padding: "5px",
                                  marginLeft: "10px",
                                }}
                              />
                              <button
                                type="button"
                                onClick={handleAddDate}
                                style={{
                                  borderRadius: "5px",
                                  padding: "4px",
                                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.3)",
                                  marginLeft: "10px",
                                }}
                              >
                                Add date
                              </button>

                              <div>
                                {selectedDates.map((date) => (
                                  <div key={date}>
                                    <span>{date}</span>
                                    <button
                                      onClick={() => handleRemoveDate(date)}
                                      style={{
                                        borderRadius: "5px",
                                        boxShadow:
                                          "0 0 10px rgba(0, 0, 0, 0.3)",
                                        padding: "4px",
                                      }}
                                    >
                                      Remove
                                    </button>
                                  </div>
                                ))}
                              </div>
                            </div>
                          </div>

                          <div
                            className={style.ubutton}
                            style={{ display: "flex", height: "30%" }}
                          >
                            <button
                              type="submit"
                              style={{
                                marginLeft: "39px",
                                marginRight: "150px",
                                backgroundColor: " #004b6b",
                              }}
                            >
                              <p> Book </p>
                            </button>
                            <button
                              style={{
                                marginRight: "150px",
                                backgroundColor: " #004b6b",
                              }}
                              onClick={() => close()}
                            >
                              <p> Cancel</p>
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  )}
                </Popup>
              </div>
            </div>
          </div>
          <table
            style={{
              width: "100%",
              marginRight: "-30px",
              marginLeft: "20px",
              backgroundColor: "white",
              borderRadius: "1px",
            }}
          >
            <thead>
              <tr
                style={{
                  position: "sticky",
                  top: 0,
                  backgroundColor: "white",
                  zIndex: 1,
                  paddingBottom: "10px",
                  height: "75px",
                  borderBottom: "1px solid green",
                }}
              >
                <th style={headerStyle}></th>
                <th style={headerStyle}>Date</th>
                <th style={headerStyle}>Time</th>
                <th style={headerStyle}>Status</th>
                <th style={headerStyle}>Action</th>
              </tr>
            </thead>
            <tbody>
              {bulkbookings &&
                bulkbookings.map((bulkbooking, index) => (
                  <tr key={bulkbooking.id} style={rowStyle}>
                    <td style={cellStyle}>{index + 1}</td>
                    <td style={cellStyle}>
                      {bulkbooking.date ? bulkbooking.date.slice(0, 10) : ""}
                    </td>
                    <td style={cellStyle}>{bulkbooking.time}</td>
                    <td style={cellStyle}>Booked</td>
                    <td style={cellStyle}>
                      <FaTrash
                        style={{ cursor: "pointer", color: "#052240" }}
                        onClick={() =>
                          confirmDelete(bulkbooking.date, bulkbooking.time)
                        }
                      />
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default BulkBooking;
const headerStyle = {
  position: "sticky",
  top: "90px",
  left: "0",
  right: "0",
  padding: "6px",
  textAlign: "center",
  backgroundColor: "#f2f2f2",
  fontWeight: "bold",
  zIndex: 1,
  height: "55px",
  backgroundColor: "white",
  // borderBottom: "1px solid green",
};

const cellStyle = {
  padding: "6px",
  textAlign: "center",
  border: "1px",
  paddingTop: "20px",
  backgroundColor: "white",
};

const rowStyle = {
  backgroundColor: "#e6f7ff",
  borderBottom: "1px solid #ddd",
  cursor: "pointer",
  transition: "background-color 0.3s",
};
