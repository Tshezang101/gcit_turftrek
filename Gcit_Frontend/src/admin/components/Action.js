import React from 'react';
import { Link } from 'react-router-dom';

function Action({ userId, label }) {
    return (
        <Link to={`/user/${userId}`}>
            <button
                style={{
                    backgroundColor: 'rgba(44, 115, 235, 1)',
                    borderRadius: 6,
                    color: 'white',
                    padding: '5px 10px',
                    cursor: 'pointer',
                }}
            >
                {label} {/* Display the button's label */}
            </button>
        </Link>
        
        
    );
}

export default Action;
