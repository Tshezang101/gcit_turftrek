import React, { useState, useEffect } from "react";
import axios from "axios";
import style from "../css/page.module.css";
import { Link } from "react-router-dom";

const ManageUser = () => {
  const [userinfo, setUserinfo] = useState([]);

  useEffect(() => {
    fetchUserinfo();
  }, []);

  const fetchUserinfo = async () => {
    try {
      const response = await axios.get("http://localhost:8080/usermanage");
      if (response.data && Array.isArray(response.data.users)) {
        setUserinfo(response.data.users);
      } else {
        console.error("Unexpected response format:", response.data);
        setUserinfo([]); // Fallback to empty array
      }
    } catch (error) {
      console.error("Error fetching user info:", error);
      setUserinfo([]); // Default to empty array on error
    }
  };

  return (
    <div className={style.body}>
      <div
        style={{
          position: "fixed",
          top: 0,
          left: 240,
          right: 0,
          height: "90px",
          backgroundColor: "white",
          zIndex: 1000,
          borderBottom: "2px solid green",
          padding: "10px 20px",
          display: "flex",
          alignItems: "center",
        }}
      >
        <h1
          style={{
            color: "#052240",
            fontWeight: "bold",
            fontSize: "22px",
            fontWeight: "bold",
            fontSize: "30px",
            textAlign: "justify",
            marginTop: "3%",
            marginLeft: "1%",
          }}
        >
          Manage User
        </h1>
      </div>
      <div className="px-3 bg-white col-lg-12" style={{ paddingTop: "60px" }}>
        <div className="container-fluid card-container">
          <div className="row g-15 my-3" style={{ marginLeft: 25 }}>
            <table
              style={{
                width: "100%",
                marginRight: "-40px",
                backgroundColor: "white",
                borderRadius: "1px",
              }}
            >
              <thead>
                <tr
                  style={{
                    position: "sticky",
                    top: 0,
                    backgroundColor: "white",
                    zIndex: 1,
                    paddingBottom: "10px",
                  }}
                >
                  <th style={headerStyle}>User Name</th>
                  <th style={headerStyle} h>
                    Email
                  </th>
                  <th style={headerStyle}> Active Status</th>
                  <th style={headerStyle}>Action</th>
                </tr>
              </thead>
              <tbody style={{ paddingTop: "40px" }}>
                {userinfo.length > 0 ? (
                  userinfo.map((user, index) => (
                    <tr key={index} style={rowStyle}>
                      <td style={cellStyle}>{user.name}</td>
                      <td style={cellStyle}>{user.email}</td>
                      <td
                        style={{
                          color: user.status === "Active" ? "green" : "red",
                          padding: "12px",
                          textAlign: "center",
                          border: "1px",
                        }}
                      >
                        {user.status}
                      </td>
                      <td style={cellStyle}>
                        <Link
                          to={
                            user.status === "Active"
                              ? `/ManageUsers/${user._id}`
                              : `/UnbanUser/${user._id}`
                          }
                        >
                          <button
                            style={{
                              width: "70px",
                              height: "35px",
                              background: "#03c03c ",
                              borderRadius: "5px",
                              border: "none",
                            }}
                          >
                            View{" "}
                          </button>
                        </Link>
                      </td>
                    </tr>
                  ))
                ) : (
                  <tr>
                    <td colSpan={4}>No users found.</td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ManageUser;
const headerStyle = {
  position: "sticky",
  top: "90px",
  left: "0",
  right: "0",
  padding: "6px",
  textAlign: "center",
  backgroundColor: "white",
  fontWeight: "bold",
  zIndex: 1,
  height: "60px",
};

const cellStyle = {
  padding: "6px",
  textAlign: "center",
  border: "1px",
  paddingTop: "30px",
  backgroundColor: "white",
};

const rowStyle = {
  backgroundColor: "white",
  borderBottom: "1px solid #ddd",
  cursor: "pointer",
  transition: "background-color 0.3s",
};
