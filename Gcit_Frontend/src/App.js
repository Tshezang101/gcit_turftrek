import { BrowserRouter, Routes, Route } from "react-router-dom";
import React from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { AuthProvider } from "./admin/contexts/AuthContext";
import PrivateRoute from "./admin/components/PrivateRoute";
import PublicRoute from "./admin/components/PublicRoute";
import AdminLogin from "./admin/auth/login";
import Main from "./admin/main";
import BookingMain from "./admin/BookingMain";
import BookingMain2 from "./admin/BookingMain2";
import BBookingMain from "./admin/BulkBookingMain";
import ManageUserMain from "./admin/manageUserMain.js";
import SettingMain from "./admin/settingMain";
import UnbanUserMain from "./admin/UnbanUserMain.js";
import ManageUsersMain from "./admin/ManageUsersMain.js";

function App() {
  return (
    <div className="App">
      <ToastContainer position="top-center" />
      <AuthProvider>
        <BrowserRouter>
          <Routes>
            <Route element={<PublicRoute restricted />}>
              <Route path="/" element={<AdminLogin />} />
            </Route>
            <Route element={<PrivateRoute />}>
              <Route path="/Dashboard" element={<Main />} />
              <Route path="/Booking" element={<BookingMain />} />
              <Route path="/Booking2" element={<BookingMain2 />} />
              <Route path="/BBooking" element={<BBookingMain />} />
              <Route path="/ManageUser" element={<ManageUserMain />} />
              <Route path="/UnbanUser/:userId" element={<UnbanUserMain />} />
              <Route
                path="/ManageUsers/:userId"
                element={<ManageUsersMain />}
              />
              <Route path="/setting" element={<SettingMain />} />
            </Route>
          </Routes>
        </BrowserRouter>
      </AuthProvider>
    </div>
  );
}
export default App;
