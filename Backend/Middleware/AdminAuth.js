
const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {
    const authHeader = req.headers.authorization;
    if (!authHeader || !authHeader.startsWith('Bearer ')) {
        return res.status(401).json({ error: 'Unauthorized: Token required' });
    }

    const token = authHeader.split(' ')[1];

    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
        if (err) {
            const message = err.name === 'JsonWebTokenError' ? 'Unauthorized: Invalid token' :
                            err.name === 'TokenExpiredError' ? 'Unauthorized: Token has expired, please login again' : 
                            'Unauthorized: Token verification failed';
            return res.status(401).json({ error: message });
        }

        req.user = decoded;  // Attach the user payload to the request object
        next();
    });
};

module.exports = verifyToken;
