const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const User = mongoose.model("User");
require("dotenv").config();

module.exports = async (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization || !authorization.startsWith("Bearer ")) {
    return res.status(401).send({ error: "You must be logged in" });
  }

  const token = authorization.split(" ")[1];
  try {
    const payload = jwt.verify(token, process.env.jwt_secret);
    const user = await User.findById(payload._id);
    if (!user) {
      return res.status(401).json({ error: "User not found" });
    }

    // Extract user's email from user data
    const userEmail = user.email;

    // Extract domain from user's email
    const domain = userEmail.split("@")[1];

    // Define the college email domain where bookings are free
    const collegeEmailDomain = "rub.edu.bt";

    // Check if user's email domain matches the college email domain
    if (domain === collegeEmailDomain) {
      req.body.paymentStatus = null;
      req.body.bankName = null;
      req.body.journalNumber = null;
    } else {
      // Otherwise, set payment status to 'unpaid' or whatever is appropriate
      req.body.paymentStatus = "unpaid";

      // If the user doesn't belong to the college, ensure the journalNumber and bankName fields are provided
      if (!req.body.journalNumber || !req.body.bankName) {
        return res
          .status(400)
          .json({ error: "Journal number and bank name are required" });
      }
    }

    // Add the user object to the request for further processing if needed
    req.user = user;

    // Add userId to the request for further processing
    req.userId = user._id;

    next();
  } catch (error) {
    console.error("Error verifying token:", error);
    return res.status(401).json({ error: "Invalid token" });
  }
};
