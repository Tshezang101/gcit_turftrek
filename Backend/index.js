const express = require("express");
const app = express();
const bodyParser = require("body-parser");
require("dotenv").config();
const mongoose = require("mongoose");
const cors = require("cors");

const authRoutes = require("./routes/authRoutes");
const bookingRoutes = require("./routes/bookingRoutes");
const availabilityController = require("./routes/availabilityRoutes");
const adminRoutes = require("./routes/adminRoutes");
const settingRoutes = require("./routes/settingRoutes");
const cloudinaryRoutes = require("./routes/cloudinaryRoutes");

// Middleware
app.use(cors());
app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));

// Database connection
mongoose
  .connect(process.env.mongo_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Connected to database"))
  .catch((err) => console.error("Database connection error:", err));

// Routes
app.use(bookingRoutes);
app.use(authRoutes);
app.use(availabilityController);
app.use(adminRoutes);
// app.use(ManageUser);
app.use(settingRoutes);

app.use(cloudinaryRoutes);

// Error handling middleware - should be placed after all other route and middleware declarations
app.use((req, res, next) => {
  res.status(404).json({ error: "Resource not found" });
});

const PORT = process.env.PORT || 8080; 
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
