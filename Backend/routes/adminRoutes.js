const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

require("dotenv").config();

// Mocked admin credentials
const adminEmail = "gcit@gmail.com";
const adminPassword = bcrypt.hashSync("adminpass", 10); // Password is hashed to maintain security

// Admin login route
router.post("/adminlogin", (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(422).send({ error: "Email and password are required" });
  }

  if (email !== adminEmail) {
    return res.status(422).send({ error: "Invalid email" });
  }

  bcrypt.compare(password, adminPassword, (err, result) => {
    if (err) {
      console.error("Error comparing passwords:", err);
      return res.status(500).send({ error: "Internal Server Error" });
    }

    if (!result) {
      return res.status(422).send({ error: "Invalid password" });
    }

    const token = jwt.sign({ isAdmin: true }, process.env.jwt_secret, {
      expiresIn: "1h",
    });

    res.send({ token });
  });
});

// Admin logout route
router.post("/adminlogout", (req, res) => {
  res.send({ message: "Logged out successfully" });
});

module.exports = router;