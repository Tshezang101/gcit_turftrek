const express = require('express');
    const router = express.Router();
    const moment = require('moment');
    const mongoose = require('mongoose');
    const Booking = require('../models/Booking');
    const Availability = require("../models/Availability")
    const { v4: uuidv4 } = require('uuid');
    const authMiddleware = require("../Middleware/AuthTokenRequired")
    const nodemailer = require('nodemailer');
    const User = require('../models/User');


// Route to fetch bookings for college users
router.post('/bookGround', authMiddleware, async (req, res) => {
    try {
        const { userId, slotId, paymentStatus, journalNumber, bankName, date, time, contact, duration } = req.body;

        // Parse the duration string to extract hours
        const durationHours = parseInt(duration); // Assuming duration is provided as a number followed by "hr"

        if (isNaN(durationHours)) {
            return res.status(400).json({ error: 'Invalid duration format' });
        }

        const user = await User.findById(userId);

        if (user.status !== 'Active') {
            return res.status(403).json({ error: 'This user is banned from booking' });
        }

        // Find the availability for the specified date
        const availability = await Availability.findOne({ 'timeSlotsWithStatus.slotId': slotId });

        if (!availability) {
            return res.status(404).json({ error: 'No availability found for the provided date' });
        }

        // Find the selected slot and check its availability
        const selectedSlotIndex = availability.timeSlotsWithStatus.findIndex(slot => slot.slotId === slotId);

        if (selectedSlotIndex === -1 || availability.timeSlotsWithStatus[selectedSlotIndex].status !== 'Available') {
            return res.status(400).json({ error: 'Selected slot is not available' });
        }

        // Check if there are enough consecutive slots available for the specified duration
        const slotsForDuration = availability.timeSlotsWithStatus.slice(selectedSlotIndex, selectedSlotIndex + durationHours);

        if (slotsForDuration.some(slot => slot.status !== 'Available')) {
            return res.status(400).json({ error: 'Not enough consecutive slots available for the specified duration' });
        }

        // Update the status of selected slots to 'Booked'
        for (let i = selectedSlotIndex; i < selectedSlotIndex + durationHours; i++) {
            availability.timeSlotsWithStatus[i].status = 'Booked';
        }

        await availability.save();

        // Determine booking status and email sending based on user's email domain
        let bookingStatus;
        let sendConfirmationEmail = true;
        if (req.user.email.endsWith('.gcit@rub.edu.bt')) {
            bookingStatus = 'Booked';
        } else if (req.user.email.endsWith('@gmail.com')) {
            bookingStatus = 'Pending';
            sendConfirmationEmail = false;
        } else {
            return res.status(400).json({ error: 'Unsupported email domain' });
        }

        // Generate a UUID for booking ID
        const bookingId = uuidv4();

        // Create a new booking instance with the generated booking ID
        const booking = new Booking({
            bookingId,
            userId,
            slotId,
            time,
            date,
            paymentStatus,
            journalNumber,
            bankName,
            contact,
            duration: `${durationHours} hr`,
            status: bookingStatus
        });

        // Save the booking to the database
        await booking.save();

        // Send booking confirmation email if required
        if (sendConfirmationEmail && bookingStatus !== 'pending') {

            const user = await User.findById(userId);
            const userEmail = user.email;

            const mailOptions = {
                from: '12220052.gcit@rub.edu.bt',
                to: userEmail,
                subject: 'Booking Confirmation',
                html: `<p>Your booking was successful!</p><p>Booking details:<br>Date: ${date}<br>Time: ${time}<br>Contact: ${contact}<br>Duration: ${duration}</p>`
            };

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.error('Error sending email:', error);
                } else {
                    console.log('Email sent:', info.response);
                }
            });
        }

        // Respond with success message and the booking details
        res.status(201).json({ message: 'Booking successful', booking });
    } catch (error) {
        console.error('Error creating booking:', error);
        res.status(500).json({ error: 'Failed to create booking' });
    }
});




    // router.get('/bookings/insider', async (req, res) => {
    //     try {
    //         const bookings = await Booking.find({})
    //             .populate('userId', 'name email');

    //         const insiderBookings = bookings.filter(booking => {
    //             const emailDomain = booking.userId.email.split('@')[1];
    //             return emailDomain == 'rub.edu.bt';
    //         });

    //         res.json(insiderBookings);
    //     } catch (error) {
    //         console.error('Error fetching insider bookings:', error);
    //         res.status(500).json({ error: 'Internal server error' });
    //     }
    // });

    router.get('/bookings/insider', async (req, res) => {
      try {
          const bookings = await Booking.find({})
              .populate('userId', 'name email');
  
          const insiderBookings = bookings.filter(booking => {
              // Check if userId is populated
              if (!booking.userId) {
                  return false;
              }
              const emailDomain = booking.userId.email.split('@')[1];
              return emailDomain == 'rub.edu.bt';
          });
  
          res.json(insiderBookings);
      } catch (error) {
          console.error('Error fetching insider bookings:', error);
          res.status(500).json({ error: 'Internal server error' });
      }
  });
  
    // Route to fetch bookings for non-college users
    // router.get('/bookings/outsider', async (req, res) => {
    //     try {
    //         const bookings = await Booking.find({})
    //             .populate('userId', 'name email');

    //         const outsiderBookings = bookings.filter(booking => {
    //             const emailDomain = booking.userId.email.split('@')[1];
    //             return emailDomain !== 'rub.edu.bt';
    //         });

    //         res.json(outsiderBookings);
    //     } catch (error) {
    //         console.error('Error fetching outsider bookings:', error);
    //         res.status(500).json({ error: 'Internal server error' });
    //     }
    // });
    router.get('/bookings/outsider', async (req, res) => {
      try {
          const bookings = await Booking.find({})
              .populate('userId', 'name email');
  
          const outsiderBookings = bookings.filter(booking => {
              // Check if userId is populated
              if (!booking.userId) {
                  return false;
              }
              const emailDomain = booking.userId.email.split('@')[1];
              return emailDomain !== 'rub.edu.bt';
          });
  
          res.json(outsiderBookings);
      } catch (error) {
          console.error('Error fetching outsider bookings:', error);
          res.status(500).json({ error: 'Internal server error' });
      }
  });
  






    router.get('/bookings/:bookingId', async (req, res) => {
        try {
            const bookingId = req.params.bookingId;
    
            // Find the booking by bookingId
            const booking = await Booking.findOne({ bookingId });
            if (!booking) {
                return res.status(404).json({ error: 'Booking not found' });
            }
            res.status(200).json({ booking });
        } catch (error) {
            console.error('Error fetching booking by ID:', error);
            res.status(500).json({ error: 'Failed to fetch booking' });
        }
    });
    
    


    router.delete('/bookings/:bookingId', async (req, res) => {
        try {
            const bookingId = req.params.bookingId; // Get bookingId from params
    
            // Find and delete the booking by bookingId
            const booking = await Booking.findOneAndDelete({ bookingId });
            if (!booking) {
                return res.status(404).json({ error: 'Booking not found' });
            }
    
            res.status(200).json({ message: 'Booking deleted successfully' });
        } catch (error) {
            console.error('Error deleting booking by bookingId:', error);
            res.status(500).json({ error: 'Failed to delete booking' });
        }
    });

    const transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        auth: {
            user: '12220052.gcit@rub.edu.bt',
            pass: '12220052'
        }
    });

    router.put('/bookings/:bookingId', async (req, res) => {
        try {
            const bookingId = req.params.bookingId;
            const { paymentStatus, date, time } = req.body;
    
            // Find the booking by bookingId
            const booking = await Booking.findOne({ bookingId }).populate('userId', 'email');
            if (!booking) {
                return res.status(404).json({ error: 'Booking not found' });
            }
    
            // Update the payment status
            booking.paymentStatus = paymentStatus;
    
            // Retrieve user's email from the booking data
            const userEmail = booking.userId.email;
    
            // Log the retrieved user email for debugging
            console.log('User email:', userEmail);
    
            // Update booking status based on paymentStatus
            if (paymentStatus === 'paid') {
                // If payment is made, set the payment status to 'paid' and booking status to 'Booked'
                booking.status = 'Booked';
    
                // Send booking confirmation email to the user
                const mailOptions = {
                    from: '12220052.gcit@rub.edu.bt',
                    to: userEmail,
                    subject: 'Booking Confirmation',
                    html: `<p>Your booking was successful!</p><p>Booking details:<br>Date: ${date}<br>Time: ${time}<br>Contact: ${booking.contact}<br>Duration: ${booking.duration}</p>`
                };
    
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        console.error('Error sending email:', error);
                    } else {
                        console.log('Email sent:', info.response);
                    }
                });
            } else if (paymentStatus === 'reject') {
                // If payment is rejected, set the payment status to 'reject' and booking status to 'Rejected'
                booking.status = 'Rejected';
    
                // Send booking rejection email to the user
                const mailOptions = {
                    from: '12220052.gcit@rub.edu.bt',
                    to: userEmail,
                    subject: 'Booking Rejection',
                    html: `<p>Your booking couldn't be processed due to an issue with the payment journal number. Please get in touch with support for more details. <>Booking details:<br>Date: ${date}<br>Time: ${time}<br>Contact: ${booking.contact}<br>Duration: ${booking.duration}</p>`
                };
    
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        console.error('Error sending email:', error);
                    } else {
                        console.log('Rejection email sent:', info.response);
                    }
                });
            }
    
            // Update date and time fields if provided
            if (date && time) {
                booking.date = date;
                booking.time = time;
            }
    
            // Save the updated booking
            await booking.save();
    
            res.status(200).json({ message: 'Booking status updated successfully', booking });
        } catch (error) {
            console.error('Error updating booking status:', error);
            res.status(500).json({ error: 'Failed to update booking status' });
        }
    });
        
        


    // Route to cancel booking and free the slot
    router.put('/bookings/cancel/:bookingId', async (req, res) => {
        try {
            const bookingId = req.params.bookingId;
            const { paymentStatus, date, time } = req.body;

    
            // Find the booking by bookingId
            const booking = await Booking.findOne({ bookingId }).populate('userId', 'email');
            if (!booking) {
                return res.status(404).json({ error: 'Booking not found' });
            }
    
            // Retrieve the slotId associated with the booking
            const slotId = booking.slotId;
            booking.paymentStatus = paymentStatus;

    
            // Update availability back to "Available" for the slot associated with the canceled booking
            const availability = await Availability.findOneAndUpdate(
                { 'timeSlotsWithStatus.slotId': slotId },
                { $set: { 'timeSlotsWithStatus.$.status': 'Available' } }
            );
    
            // Update booking status to 'Cancelled' and payment status to 'cancel'
            if (paymentStatus === 'cancel') {
                // If payment is made, set the payment status to 'paid' and booking status to 'Booked'
                booking.status = 'Cancelled';    
            // Send cancellation email to the user
                const mailOptions = {
                    from: '12220052.gcit@rub.edu.bt',
                    to: booking.userId.email,
                    subject: 'Booking Cancellation',
                    html: `<p>Your booking has been successfully canceled.</p>`
                };
        
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        console.error('Error sending email:', error);
                    } else {
                        console.log('Cancellation email sent:', info.response);
                    }
                });
            }
            if (date && time) {
                booking.date = date;
                booking.time = time;
            }
            // Save the updated booking to the database
            await booking.save();
    
            res.status(200).json({ message: 'Booking canceled successfully', booking });
        } catch (error) {
            console.error('Error canceling booking:', error);
            res.status(500).json({ error: 'Failed to cancel booking' });
        }
    });
    


    module.exports = router;