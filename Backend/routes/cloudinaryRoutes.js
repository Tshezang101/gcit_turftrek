const express = require('express');
const router = express.Router();
const cloudinary = require('../cloudinary/cloudinary')

router.post("/uploadImage", async (req, res) => {
    try {
        const { image } = req.body;
        const uploadedImage = await cloudinary.uploader.upload(image, {
            upload_preset: 'j6nboc6g',
            folder: 'GCIT_Turftrek',
            allowed_formats: ['png', 'jpg', 'jpeg', 'svg', 'ico', 'jfif', 'webp']
        });
        // Send success response with uploaded image details
        res.status(200).json(uploadedImage);
    } catch (error) {
        // Send error response if upload fails
        console.error("Error uploading image:", error);
        res.status(500).json({ error: "Failed to upload image" });
    }
});

module.exports = router;
