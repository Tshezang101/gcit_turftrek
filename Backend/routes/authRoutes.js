const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const User = require("../models/User");

require("dotenv").config();



router.post("/signup", async (req, res) => {
  console.log("sent by client", req.body);
  const { name, email, password, confirmpassword } = req.body;

  if (!name || !email || !password || !confirmpassword) {
    return res.status(422).send({ error: "Please fill all the fields" });
  }

  if (password !== confirmpassword) {
    return res.status(422).send({ error: "Passwords do not match" });
  }

  // Generate a random OTP and set OTP expiry time to 10 minutes from now
  const otp = Math.floor(Math.random() * 9000 + 1000);
  const otpExpiry = Date.now() + 600000; // 10 minutes from now

  const mailOptions = {
    from: "12220052.gcit@rub.edu.bt",
    to: email,
    subject: "OTP Verification",
    text: `Your OTP is ${otp}`,
  };

  transporter.sendMail(mailOptions, async (err, info) => {
    if (err) {
      console.log("err", err);
      return res.status(422).send({ error: "Error sending email" });
    }

    // Save the user with a pending status, OTP, and OTP expiry time
    const hashedPassword = await bcrypt.hash(password, 10);
    const user = new User({
      name,
      email,
      password: hashedPassword,
      verified: false, // Setting verified status to false
      otp,
      otpExpiry,
    });

    try {
      await user.save();
      console.log(otp);
      res.send({ message: "OTP sent to your email" });
    } catch (err) {
      console.log("db.err", err);
      return res.status(422).send({ error: err.message });
    }
  });
});

router.post("/verify-otp", async (req, res) => {
  try {
    const { email, otp } = req.body;

    if (!email || !otp) {
      return res.status(422).send({ error: "Please fill all the fields" });
    }

    const user = await User.findOne({ email });

    if (!user || user.otp !== otp || Date.now() > user.otpExpiry) {
      return res.status(422).send({ error: "Invalid OTP or OTP expired" });
    }

    // Mark the user as verified and clear OTP fields
    user.verified = true;
    user.otp = undefined;
    user.otpExpiry = undefined;

    await user.save();

    res.send({ message: "Email verified successfully" });
  } catch (err) {
    console.log("Error in verifying OTP:", err);
    return res.status(500).send({ error: "Internal server error" });
  }
});

router.post("/login", async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    return res.status(422).send({ error: "Credentials required" });
  }

  try {
    const user = await User.findOne({ email });

    if (!user || !user.verified) {
      return res
        .status(422)
        .json({ error: "Please verify your email address first" });
    }

    bcrypt.compare(password, user.password, (err, result) => {
      if (result) {
        console.log("Password matched");
        const token = jwt.sign({ _id: user._id }, process.env.jwt_secret);
        return res.send({ token });
      } else {
        console.log("Password does not match");
        return res.status(422).json({ error: "Invalid Credentials" });
      }
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Internal server error" });
  }
});

router.get("/usercount", async (req, res) => {
  try {
    // Fetch GCIT users
    const gcitUsersCount = await User.countDocuments({
      email: /.*\.gcit@rub\.edu\.bt$/,
    });

    // Fetch non-GCIT users
    const nonGcitUsersCount = await User.countDocuments({
      email: { $not: /.*\.gcit@rub\.edu\.bt$/ },
    });

    // Return counts
    res.status(200).json({
      gcitUsersCount,
      nonGcitUsersCount,
      totalUsers: gcitUsersCount + nonGcitUsersCount,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Error fetching user counts" });
  }
});
router.get("/usersinfo", async (req, res) => {
  try {
    // Find all users and select only name and email fields
    const users = await User.find({}, "name email");

    // If no users found, send 404 response
    if (!users || users.length === 0) {
      return res.status(404).json({ error: "No users found" });
    }

    // Send back names and emails of all users
    const userInfo = users.map((user) => ({
      name: user.name,
      email: user.email,
    }));
    res.json(userInfo);
  } catch (error) {
    console.error("Error fetching users info:", error);
    res.status(500).json({ error: "Failed to fetch users info" });
  }
});

router.post("/login", async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(422).json({ error: "Credentials required" });
  }

  try {
    const savedUser = await User.findOne({ email });

    if (!savedUser) {
      return res.status(422).json({ error: "Invalid Credentials" });
    }

    bcrypt.compare(password, savedUser.password, (err, result) => {
      if (result) {
        console.log("Password match");
        const token = jwt.sign({ _id: savedUser._id }, process.env.jwt_secret);
        res.send({ token });
      } else {
        console.log("Password does not match");
        return res.status(422).json({ error: "Invalid Credentials" });
      }
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({ error: "Internal Server Error" });
  }
});
router.get("/userdetails", async (req, res) => {
  try {
    // Fetch all GCIT users (users with email ending in .gcit@rub.edu.bt)
    const gcitUsers = await User.find(
      { email: /.*\.gcit@rub\.edu\.bt$/ },
      "email name"
    );

    // Fetch all non-GCIT users (users with email not ending in .gcit@rub.edu.bt)
    const nonGcitUsers = await User.find(
      { email: { $not: /.*\.gcit@rub\.edu\.bt$/ } },
      "email name"
    );

    // Return the user details
    res.status(200).json({
      gcitUsers,
      nonGcitUsers,
      totalUsers: gcitUsers.length + nonGcitUsers.length,
    });
  } catch (error) {
    console.error("Error fetching user details:", error);
    res.status(500).json({ message: "Server error" });
  }
});

router.get("/usermanage", async (req, res) => {
  try {
    // Fetch all users, selecting only email and name
    const users = await User.find({}, "email name status");

    // Return the list of user details
    res.status(200).json({
      users,
    });
  } catch (error) {
    console.error("Error fetching user details:", error);
    res.status(500).json({ message: "Server error" });
  }
});
router.get("/user/:userId", async (req, res) => {
  try {
    const { userId } = req.params; // Extract user ID from the URL parameters
    const user = await User.findById(userId, "name email"); // Fetch user with specified fields

    if (!user) {
      return res.status(404).json({ message: "User not found" }); // Handle user not found
    }

    // Return user details
    res.status(200).json(user);
  } catch (error) {
    console.error("Error fetching user by ID:", error);
    res.status(500).json({ message: "Server error" }); // Handle server error
  }
});



const transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 587,
  auth: {
    user: "12220052.gcit@rub.edu.bt",
    pass: "12220052",
  },
});

router.post("/users/ban/:id", async (req, res) => {
  const { id } = req.params;
  const { reason } = req.body;

  try {
    const user = await User.findById(id).populate("email");

    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }

    user.status = "Banned";
    user.reasonForBanning = reason; // Saving the reason for banning
    const userEmail = user.email;

    const mailOptions = {
      from: "12220052.gcit@rub.edu.bt",
      to: userEmail,
      subject: "Ban user",
      html: `<p>You have been been banned. You cannot book the ground from now</p>`,
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.error("Error sending email:", error);
      } else {
        console.log("Email sent:", info.response);
      }
    });

    await user.save();

    res.json({
      message: "User banned successfully",
      reasonForBanning: user.reasonForBanning,
    });
  } catch (error) {
    console.error("Error banning user:", error);
    res.status(500).json({ error: "Server error" });
  }
});

router.get("/user/reason/:userId", async (req, res) => {
  try {
    const { userId } = req.params;
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Return user details with reasonForBanning field
    const userDetails = {
      _id: user._id,
      name: user.name,
      email: user.email,
      status: user.status,
      reasonForBanning: user.reasonForBanning,
    };

    res.status(200).json(userDetails);
  } catch (error) {
    console.error("Error fetching user by ID:", error);
    res.status(500).json({ message: "Server error" });
  }
});

// Route to unban a user
router.post("/users/unban/:id", async (req, res) => {
  const { id } = req.params;

  try {
    const user = await User.findById(id);

    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }

    user.status = "Active";
    user.reasonForBanning = null;
    const userEmail = user.email;

    const mailOptions = {
      from: "12220052.gcit@rub.edu.bt",
      to: userEmail,
      subject: "Unban user",
      html: `<p>You have been unbanned. You can now book the ground again.</p>`,
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.error("Error sending email:", error);
      } else {
        console.log("Email sent:", info.response);
      }
    });

    await user.save();

    res.json({ message: "User unbanned successfully" });
  } catch (error) {
    console.error("Error unbanning user:", error);
    res.status(500).json({ error: "Server error" });
  }
});


module.exports = router;
