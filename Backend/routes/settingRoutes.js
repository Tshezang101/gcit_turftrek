const express = require('express');
const router = express.Router();
const Price = require('../models/price');

// GET route to fetch pricing data
router.get('/price', async (req, res) => {
  try {
    const prices = await Price.findOne(); // Assuming only one set of prices exists
    res.json(prices);
  } catch (error) {
    console.error('Error fetching pricing data:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// PATCH route to update pricing data
router.patch('/price', async (req, res) => {
  try {
    const { daytime, nighttime, dayprice, nightprice } = req.body;
    const prices = await Price.findOne(); // Assuming only one set of prices exists
    prices.daytime = daytime;
    prices.nighttime = nighttime;
    prices.dayprice = dayprice;
    prices.nightprice = nightprice;
    await prices.save();
    res.json({ message: 'Prices updated successfully' });
  } catch (error) {
    console.error('Error updating pricing data:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

module.exports = router;
