const express = require('express');
const router = express.Router();
const moment = require('moment');
const mongoose = require('mongoose');

const Booking = require('../models/Outsider');

// Route to create a new booking
router.post('/outsiderBookGround', async (req, res) => {
  try {
    const { name, email, contact, date, time, journal_number, bank, status } = req.body;

    // Validate the time format
    if (!moment(time, ["h:mm a"], true).isValid()) {
      throw new Error('Invalid time format');
    }
  
    // Format the time using Moment.js
    const formattedTime = moment(time, ["h:mm a"]).format("HH:mm");

    const outsiderbooking = new Booking({
      name,
      email,
      contact,
      date,
      time: formattedTime,
      journal_number,
      bank,
      status
    });

    await outsiderbooking.save();

    res.status(201).json({ message: 'Outsider Booking successful', outsiderbooking });
  } catch (error) {
    console.error('Error creating outsider booking:', error);
    res.status(500).json({ error: 'Failed to create outsider booking' });
  }
});

// Route to fetch all bookings
router.get('/outsiderbookings', async (req, res) => {
  try {
    const outsiderbookings = await Booking.find();
    res.status(200).json({ outsiderbookings });
  } catch (error) {
    console.error('Error fetching outsider bookings:', error);
    res.status(500).json({ error: 'Failed to fetch outsider bookings' });
  }
});

// Route to fetch a booking by email
router.get('/outsiderbookings/:email', async (req, res) => {
  try {
    const { email } = req.params;
    const bookings = await Booking.find({ email });
    res.status(200).json({ bookings });
  } catch (error) {
    console.error('Error fetching outsider booking by email:', error);
    res.status(500).json({ error: 'Failed to fetch outsider booking' });
  }
});

// Route to fetch a single booking by ID
router.get('/outsiderbookings/:email', async (req, res) => {
  try {
    const { email } = req.params;
    const booking = await Booking.findById(email);
    if (!booking) {
      return res.status(404).json({ error: 'Outsider booking not found' });
    }
    res.status(200).json({ booking });
  } catch (error) {
    console.error('Error fetching outsider booking by ID:', error);
    res.status(500).json({ error: 'Failed to fetch outsider booking' });
  }
});

// Route to update a booking by email
router.put('/outsiderbookings/:email', async (req, res) => {
  try {
    const { email } = req.params;
    const { name, contact, date, time, journal_number, bank, status } = req.body;

    // Validate the time format
    if (!moment(time, ["h:mm a"], true).isValid()) {
      throw new Error('Invalid time format');
    }
  
    // Format the time using Moment.js
    const formattedTime = moment(time, ["h:mm a"]).format("HH:mm");

    const outsiderbooking = await Booking.findOneAndUpdate({ email }, {
      name,
      contact,
      date,
      time: formattedTime,
      journal_number,
      bank,
      status

    }, { new: true });

    if (!outsiderbooking) {
      return res.status(404).json({ error: 'Outsider booking not found' });
    }

    res.status(200).json({ message: 'Outsider booking updated successfully', outsiderbooking });
  } catch (error) {
    console.error('Error updating outsider booking by email:', error);
    res.status(500).json({ error: 'Failed to update outsider booking' });
  }
});

// Route to delete a booking by email
router.delete('/outsiderbookings/:email', async (req, res) => {
  try {
    const { email } = req.params;

    const booking = await Booking.findOneAndDelete({ email });
    if (!booking) {
      return res.status(404).json({ error: 'Outsider booking not found' });
    }

    res.status(200).json({ message: 'Outsider booking deleted successfully' });
  } catch (error) {
    console.error('Error deleting outsider booking by email:', error);
    res.status(500).json({ error: 'Failed to outsider delete booking' });
  }
});

module.exports = router;
