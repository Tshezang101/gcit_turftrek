// controllers/availabilityController.js
const Availability = require('../models/Availability');

exports.setAvailability = async (req, res) => {
    try {
        const { date, timeSlotsWithStatus } = req.body;

        // Check if availability for the date already exists
        let availability = await Availability.findOne({ date });

        if (availability) {
            // If availability for the date exists, return a message indicating that the date slots already exist
            return res.status(400).json({ error: 'Availability slots for this date already exist' });
        }

        // Create a new availability instance
        availability = new Availability({ date, timeSlotsWithStatus });

        // Save the availability to the database
        await availability.save();

        // Respond with success message
        res.status(201).json({ message: 'Availability set successfully', availability });
    } catch (error) {
        console.error('Error setting availability:', error);
        res.status(500).json({ error: 'Failed to set availability'});
    }
};
