// src/controllers/bookingController.js

const outsiderBookings = require('../models/Outsider');

// Fetch all bookings
const getOutsiderBookings = async (req, res) => {
    try {
        const bookings = await outsiderBookings.find();
        res.status(200).json({ success: true, data: bookings });
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: 'Server Error' });
    }
};

module.exports = { getOutsiderBookings };
    