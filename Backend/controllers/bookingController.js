// src/controllers/bookingController.js

const Booking = require('../models/Booking');

// Fetch all bookings
const getBookings = async (req, res) => {
    try {
        const bookings = await Booking.find();
        res.status(200).json({ success: true, data: bookings });
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: 'Server Error' });
    }
};

module.exports = { getBookings };
