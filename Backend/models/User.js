const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  confirmpassword: {
    type: String,
    required: false,
  },
  verified: {
    type: Boolean,
    default: true,
  },
  otp: {
    type: String,
  },
  status: {
    type: String,
    default: "Active",
    
  },
  reasonForBanning: {
    type: String,
    default: null,
  },
});

const User = mongoose.model("User", userSchema);

module.exports = User;
