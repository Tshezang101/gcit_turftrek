const { duration } = require("moment");
const mongoose = require("mongoose");

const bookingSchema = new mongoose.Schema({
  bookingId: { type: String, required: true, unique: true },

  userId: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
  slotId: { type: String, required: true },
  paymentStatus: {
    type: String,
    enum: ["paid", "unpaid", "reject", "cancel"],
    default: "unpaid",
  },
  journalNumber: {
    type: Number,
    default: null,
    validate: {
      validator: function () {
        return this.userId && this.userId.email !== "gcit.@rub.edu.bt";
      },
      message: "Journal number is required for non-college users",
    },
  },
  bankName: {
    type: String,
    default: null,
    validate: {
      validator: function () {
        return this.userId && this.userId.email !== "gcit.@rub.edu.bt";
      },
      message: "Bank name is required for non-college users",
    },
  },
  status: {
    type: String,
    enum: ["Pending", "Booked", "Rejected", "Cancelled"], // Include 'Rejected' in the enum values
    default: "Pending",
    validate: {
      validator: function (value) {
        // Check if the value is one of the allowed enums
        return ["Pending", "Booked", "Rejected", "Cancelled"].includes(value);
      },
    },
  },

  date: { type: Date },
  time: { type: String },
  contact: { type: Number },
  duration: { type: String },
});

// bookingSchema.pre('save', async function (next) {
//     // If payment is verified or user is from GCIT, update status to 'Booked'
//     if ((this.isModified('paymentStatus') && this.paymentStatus === 'paid') ||
//         (this.userId && this.userId.email && this.userId.email === 'gcit.rub.edu.bt')) {
//         this.status = 'Booked';
//     }
//     next();
// });
bookingSchema.pre("save", async function (next) {
  try {
    // Ensure that the user is populated
    await this.populate("userId");

    // Check if the user is from GCIT
    const emailDomain = this.userId.email.split("@")[1];
    if (emailDomain === "gcit.rub.edu.bt") {
      // Set status to 'Booked' for GCIT users
      this.status = "Booked";
    }
    next();
  } catch (error) {
    console.error("Error in pre-save hook:", error);
    next(error);
  }
});

bookingSchema.pre("findOne", function (next) {
  this.populate("slotId");
  next();
});

bookingSchema.post("findOne", function (booking) {
  if (booking && booking.slotId) {
    booking.date = booking.slotId.date;
    booking.time = booking.slotId.time;
  }
});

module.exports = mongoose.model("Booking", bookingSchema);
