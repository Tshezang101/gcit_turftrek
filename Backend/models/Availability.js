const mongoose = require('mongoose');

const availabilitySchema = new mongoose.Schema({
    date: { type: Date, required: true },
    timeSlotsWithStatus: [{
        _id: false, // Disable automatic generation of _id for subdocuments
        time: { type: String, required: true }, // Time of the slot
        status: { type: String, enum: ['Available', 'pending', 'Booked', 'default'], default: 'Available' }, // Status of the slot
        slotId: { type: String, required: true } // Slot ID combining date and time
    }]
});

// Pre-save hook to add slotId to each time slot
availabilitySchema.pre('save', function (next) {
    this.timeSlotsWithStatus.forEach(slot => {
        // Combine date and time to create a unique slot ID
        const slotId = `${this.date.toISOString().split('T')[0]}-${slot.time}`;
        slot.slotId = slotId;
    });
    next();
});

module.exports = mongoose.model('Availability', availabilitySchema);