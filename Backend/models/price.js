const mongoose = require('mongoose');

// Define schema for pricing data
const priceSchema = new mongoose.Schema({
  daytime: String,
  nighttime: String,
  dayprice: Number,
  nightprice: Number
});

// Create model from schema
const Price = mongoose.model('Price', priceSchema);

module.exports = Price;
