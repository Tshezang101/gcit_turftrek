const mongoose = require('mongoose');

// Create a schema for a basic user profile with email, name, and status
const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true, // Ensures email is unique
    },
    name: {
        type: String,
        required: true, // Name is required
    },
    status: {
        type: String,
        default: 'Active', // Default status is 'Active'
    },
});

// Create a User model from the schema
const User = mongoose.model('User', userSchema);

// Export the User model
module.exports = User;
