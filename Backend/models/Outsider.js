const mongoose = require('mongoose');

const outsiderBookingSchema = new mongoose.Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    contact: { type: Number, required: true },
    date: { type: Date, required: true },
    time: { type: String, required: true },
    journal_number: { type: Number, required: true },
    bank: { type: String, required: true },
    status: { type: String, required: true },
    isConfirmed: { type: Boolean, default: false }
});

module.exports = mongoose.model('outsiderBookings', outsiderBookingSchema);
